/* eslint-disable no-console */
import { LightningElement, wire, track } from 'lwc';
import { getPicklistValues, } from 'lightning/uiObjectInfoApi';
import ACCOUNT_SOURCE from '@salesforce/schema/Account.AccountSource';

/*
    getPicklistValues -
        RecordtypeId - Required... 
        fieldApiName - Required
*/
/*
    getPicklistValuesByRecordType
        recordTypeId - Required Type Id (Real One)
        objectApiName - API Name of your Object
*/
export default class PicklistDemo extends LightningElement {
    @track pickListvalues;
    @track error;
    @track values;
    @track pickListvaluesByRecordType;
    @track accountsource;

    @wire(getPicklistValues, {
        recordTypeId : '012000000000000AAA',
        fieldApiName : ACCOUNT_SOURCE
    })
        wiredPickListValue({ data, error }){
            if(data){
                console.log(` Picklist values are `, data.values);
                this.pickListvalues = data.values;
                this.error = undefined;
            }
            if(error){
                console.log(` Error while fetching Picklist values  ${error}`);
                this.error = error;
                this.pickListvalues = undefined;
            }
        
   
        }
 
    handleChange(){
        
    }

}