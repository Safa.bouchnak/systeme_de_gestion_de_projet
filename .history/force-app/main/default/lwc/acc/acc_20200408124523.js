import { LightningElement, api } from "lwc";

import NAME_FIELD from "@salesforce/schema/lda_Opportunity__c.Name";
import ACCOUNT_FIELD from "@salesforce/schema/lda_Opportunity__c.AccountId__c";

export default class acc extends LightningElement {
  @api recordId;
  @api objectApiName;

  objectApiName = "lda_Opportunity__c";
  fields = [NAME_FIELD, ACCOUNT_FIELD];
}