import { LightningElement } from 'lwc';

export default class LwcTestDragANDDrop extends LightningElement {

    constructor() {
        super();
        //register dragover event to the template
        //this.template.addEventListener('dragover', this.allowDrop.bind(this));
        console.log('constructor');
    }


    allowDrop(ev) {

        console.log('over drag ');
        ev.preventDefault();
    }

    dropped(ev) {                  
        console.log('on drop ');

        ev.preventDefault();
        /*let data = ev.dataTransfer.getData("text");
        console.log('strt drag ');*/
    }

    dragged(ev) {
        console.log('dragg start ');
        ev.dataTransfer.setData("Text", 'ev.target.id');
    }

    enddragged(ev) {
        console.log('dragg end ');
    }

    dragEnter(event) {
        console.log(12);
        event.target.style.border = "3px dotted red";
    }

    onhover(){
        console.log('on hover');
    }
}