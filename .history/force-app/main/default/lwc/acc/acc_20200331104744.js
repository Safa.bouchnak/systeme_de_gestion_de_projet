import { LightningElement ,wire,track,api} from 'lwc';
import { getPicklistValues } from 'lightning/uiObjectInfoApi';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import CREDITCARD_OBJECT from '@salesforce/schema/Credit_card__c';
import PROGRAMM_Field from '@salesforce/schema/Credit_card__c.Choose_Program_Reason__c';
import AXPID_Field from '@salesforce/schema/Credit_card__c.AX_PID__c';
import EXCHANERATE_FIELD from '@salesforce/schema/Credit_card__c.Exchange_Rate__c';
import IMPORT_FIELD from '@salesforce/schema/Credit_card__c.ImportId__c';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import insertdata from '@salesforce/apex/importdata.readCSVFileInsertMember';
const columns = [
    { label: 'CampaignId', fieldName: 'CampaignId' }, 
   { label: 'ContactId', fieldName: 'ContactId' },
   { label: 'Status', fieldName: 'Status'}
];
export default class Uploadfilewithinput extends LightningElement {
    Credicard=CREDITCARD_OBJECT;
   ExchaneRate=EXCHANERATE_FIELD;
   ImpotId=IMPORT_FIELD;
    @api impnum ="";
    inpputnum="";
    Programvalue="";
    Avalue="";
    @track success;
    @track failure;
    @api recordId;
   @track data;
   @track columns =columns;
handlechange(event){
 this.impnum=event.target.value;
}
handlechange1(event){
this.inpputnum=event.target.value;
}
   @wire(getObjectInfo, { objectApiName: CREDITCARD_OBJECT })
   objectInfo; 
   @wire(getPicklistValues, { recordTypeId: '012000000000000AAA', fieldApiName:PROGRAMM_Field})
    Programvalues;
      @wire(getPicklistValues, { recordTypeId: '012000000000000AAA', fieldApiName: AXPID_Field})
      AXvalues;
    @wire(insertdata , { impnum: '$impnum'})
    deWired;
      inpputnum: '$inpputnum',Programvalue: '$Programvalue',Avalue: '$Avalue'
  
    handleselect(event){
      
       if(event.target.name ==='Program'){
          this.Programvalue=event.target.value;
     }
       if(event.target.name === 'AXvalue'){
           this.Avalue=event.target.value;
        }
}
get acceptedFormats() {
    return ['.csv'];
}
handleUploadFinished(event) {
    const uploadedFiles = event.detail.files;
   insertdata({idContentDocument : uploadedFiles[0].documentId}) 
    .then(result => {
      this.success = result;  
     this.dispatchEvent(
        new ShowToastEvent({
           title: 'Success!!',
            message: 'Campaign Members are created based CSV file!!!',
    variant: 'success',
        }),
    );
    })
    .catch(error => {
        this.failure=error;
        this.dispatchEvent(
            new ShowToastEvent({
              title: 'Error!!',
                message: JSON.stringify(error),
                variant: 'error',
            }),
        );     
   });
    
}
}