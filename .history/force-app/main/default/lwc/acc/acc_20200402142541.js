
import { LightningElement, api, track } from "lwc";
const columns = [
  { label: "dateIssueds", fieldName: "dateIssued", sortable:true},
  { label: "payee", fieldName: "payee", type: "text" },
  { label: "amount", fieldName: "amount", type: "currency" },
  { label: "status", fieldName: "status", type: "text", sortable: true },
  { label: "chequeEFT", fieldName: "chequeEFT", type: "text" },
  { label: "description", fieldName: "description", type: "text" }
];
export default class Payment_section extends LightningElement {
  @api payres=[];
  @track columns = columns;
  @api var;
  @track sortedBy; 
 // @track paymentdata = this.payres0[0].paymentsMap[0];
  @track sortedDirection = 'asc';
 sortData(fieldName, sortDirection){
  // if (!this.payres[0].paymentsMap) {
   let data = JSON.parse(JSON.stringify(this.payres));//this.opportunities;
   window.console.log('-->' +data);
    //function to return the value stored in the field
    let key =(a) => a[fieldName];
   let reverse = sortDirection === 'asc' ? 1: -1;
    data.sort((a,b) => {
        let valueA = key(a) ? key(a).toLowerCase() : '';
        let valueB = key(b) ? key(b).toLowerCase() : '';
        return reverse * ((valueA > valueB) - (valueB > valueA));
    });
   //set sorted data to opportunities attribute
    this.payres= data;
   window. console.log(this.payres);
}
  
handleSortdata(event){
    window .console.log(' reddy');
    this.sortedBy = event.detail.fieldName;
    this.sortedDirection = event.detail.sortDirection;
    window.console.log(this.sortedDirection);
    this.sortData(this.sortedBy,this.sortedDirection);      
}

}