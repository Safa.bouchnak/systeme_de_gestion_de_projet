import { LightningElement, wire, api, track } from 'lwc';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import PROJET_FIELD from '@salesforce/schema/Sprint__c.projet__c';
import NAME_FIELD from '@salesforce/schema/Sprint__c.Name';
import FINDATE_FIELD from '@salesforce/schema/Sprint__c.Date_Fin__c';
import FIRSTDATE_FIELD from '@salesforce/schema/Sprint__c.Date_Debut__c';
import saveSprintRecord from '@salesforce/apex/sprintControler.saveSprintRecord'; 
import DESCRIPTION_FIELD from '@salesforce/schema/Sprint__c.Description__c';
import deleteSprint from '@salesforce/apex/sprintControlerLWC.deleteSprint' ;
import getSprint from '@salesforce/apex/sprintControlerLWC.getSprint' ;
import {refreshApex} from '@salesforce/apex';
import { CurrentPageReference } from 'lightning/navigation';
import { registerListener, unregisterAllListeners } from 'c/pubsub';
import { NavigationMixin } from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';
import getProjetWorkTime from '@salesforce/apex/workTimeProjet.getProjetWorkTime'; 
import updateProjetWorkTime from '@salesforce/apex/workTimeProjet.updateProjetWorkTime'; 
const columns = [
{type: "button-icon", typeAttributes: {  
        iconName: 'action:share',
        label: 'record_Apex',  
        name: 'record_Apex',  
        title: 'record_Apex',  
        disabled: false,  
        value: 'record_Apex',  
        iconPosition: 'right',
} 
},  
{label: 'Sprint list', fieldName: 'Name' },
{label:'Description',fieldName:'Description__c'},
{label:'Start date',fieldName:'Date_Debut__c'},
{label:' End date',fieldName:'Date_Fin__c'},

{type: "button-icon", typeAttributes: {  
    label: 'delete',  
    name: 'delete',  
    title: 'delete',  
    disabled: false,  
    value: 'delete',  
    iconName: 'action:delete',
    iconPosition: 'left'  
}},
{type: "button-icon", typeAttributes: {  
    label: 'Edit',  
    name: 'edit',  
    title: 'edit' , 
    disabled: false,  
    value: 'edit',  
    iconName: 'action:edit',
    iconPosition: 'left'  
}},
{type: "button-icon" , typeAttributes: {  
    label: 'record_details',  
    name: 'record_details',  
    title: 'record_details',  
    disabled: false,  
    value: 'record_details',  
    iconName: 'action:preview',
    iconPosition: 'left'  
    
} },
{type: "button-icon", typeAttributes: {  
    iconName: 'action:share',
    label: 'Description',  
    name: 'Description',  
    title: 'Description',  
    disabled: false,  
    value: 'Description',  
    iconPosition: 'Description' , 
}}];
export default class ListeSprint  extends NavigationMixin(LightningElement){
    @api Sprint__c;
    @api recordId;
    selectedRecords = [];
    refreshTable;  
    timestamp = new Date();
    @track columns = columns;
    @track showpages =false;
    @track bShowModal = false;
    @track recordId;
    @track currentRecordId;
    @track isEditForm = false;
    @track showLoadingSpinner = false;
    @track DetailProjet;
    @track detailSprint;
    @track data;
    @track aya = true;
    @track record = [];
    @track error; 
    @track SessionRecord1;
    @track openmodel = false;
    @track SprintRecord={
        Name : NAME_FIELD,
        Description__c :DESCRIPTION_FIELD,
        projet__c:PROJET_FIELD,
        Date_Debut__c:FIRSTDATE_FIELD,
        Date_Fin__c:FINDATE_FIELD,
    };
    @wire(CurrentPageReference) pageRef;
    navigateTolisteuserstory(event) {
        const record = event.detail.row;
    fireEvent(this.pageRef, 'sendmessage',{story: record.Id});
        this[NavigationMixin.Navigate]({
            type: 'standard__navItemPage',
            attributes: {
                apiName: 'Liste_des_user_story',
                recordSprint: record,
                recordP :this.DetailProjet,
            },
        });
    }
    handleNavigateToProjet(event) {
        event.preventDefault();
        this[NavigationMixin.Navigate]({
            type: 'standard__navItemPage',
            attributes: {
                apiName:   'liste_des_projet',
            },
        });
    }
    handleNameuserchange(event){
        this.SprintRecord.Name= event.target.value;
    }
    handleDescriptionchange(event){
        this.SprintRecord.Description__c= event.target.value;
    }
    handleDateDebutuserchange(event){
        this.SprintRecord.Date_Debut__c= event.target.value;
    }
    handleDateFinchange(event){
        this.SprintRecord.Date_Fin__c= event.target.value;
    }
    openmodal() {
        this.openmodel = true;
    }
    closeModal() {
        this.openmodel = false;
    } 
    saveMethod() {
        saveSprintRecord({SpObjAcc: this.SprintRecord, projObj: this.DetailProjet})
                    .then (result => {
                        this.SprintRecord ={};
                    this.dispatchEvent(new ShowToastEvent({
                        title:'Success!',
                        message: 'User Story created successfully!',
                        variant: 'Success!',
                    }),);
                       // refreshing table data using refresh apex
                return refreshApex(this.refreshTable);
                    })
                    .catch(error => {
                        this.error =error.message;
                    });
                this.closeModal();
            }
            handleSuccess() {
                return refreshApex(this.refreshTable);
            }
            handleRowActions(event) {
            let actionName = event.detail.action.name;
            window.console.log('actionName ====> ' + actionName);
            let row = event.detail.row;
            window.console.log('row ====> ' + row);
            // eslint-disable-next-line default-case
            switch (actionName) {
                case 'recordId':
                    this.navigateToViewRecordPage(event);
                    break;
                case 'record_Apex':
                this.navigateTolisteuserstory(event);
                break;
                case 'record_details':
                    this.viewCurrentRecord(row);
                    console.log("row!" +JSON.stringify(row));
                    break;
                case 'edit':
                    this.editCurrentRecord(row);
                    break;
                case 'delete':
                    this.deleteSprint(row);
                    break;
                    case 'Description':
                        this.showpage(row);
                        break;
            }
        }
        editRecord(){
            this.editCurrentRecord(this.record);
        }
    showpage(currentRow){
    this.showpages=true;
    this.idd = currentRow.Id;
    this.record = currentRow;
    console.log('result==>',JSON.stringify( this.DetailProjet));
    getProjetWorkTime({PObj:this.DetailProjet.Id})
    .then (result => {
    updateProjetWorkTime({PObj:this.DetailProjet,SumTim: result})
})
}
@api refresh() {
    this.timestamp = new Date();
}
 //Navigate to Reports
 navigateToReports() {
    this[NavigationMixin.Navigate]({
        type: 'standard__objectPage',
        attributes: {
            objectApiName: 'Event',
            actionName: 'home'
        },
    });
}
    tableSprint() {    
        getSprint({ projObj:this.DetailProjet})
            .then (result => {
                this.refreshTable = result;
            this.SprintRecord ={};
                this.data = result;
            })}
            viewCurrentRecord(currentRow) {
                this.bShowModal = true;
                this.isEditForm = false;
                this.record = currentRow;
            }
            closeModalAction() {
                this.bShowModal = false;
            }
            editCurrentRecord(currentRow) {
                // open modal box
                this.bShowModal = true;
                this.isEditForm = true;
        
                // assign record id to the record edit form
                this.currentRecordId = currentRow.Id;
            }
            handleSubmit(event) {
                // prevending default type sumbit of record edit form
                event.preventDefault();
                // querying the record edit form and submiting fields to form
                this.template.querySelector('lightning-record-edit-form').submit(event.detail.fields);
                // closing modal
                this.bShowModal = false;
                // showing success message
                this.dispatchEvent(new ShowToastEvent({
                    title: 'Success!',
                    message: event.detail.fields.Name + 'User Story successfully updated !!. ',
                    variant: 'Success!'
                }),);
                return refreshApex(this.refreshTable);
            }
            deleteSprint(currentRow) {
                let currentRecord = [];
                currentRecord.push(currentRow.Id);
                this.showLoadingSpinner = true;
                // calling apex class method to delete the selected contact
                deleteSprint({lstSprintIds: currentRecord})
                .then(result => {
                    window.console.log('result ====> ' + result);
                    this.showLoadingSpinner = false;
                    // showing success message
                    this.dispatchEvent(new ShowToastEvent({
                        title: 'Success!',
                        message: currentRow.Name +'Sprint Removed.',
                        variant: 'Success!'
                    }),);
                    // refreshing table data using refresh apex
                    return refreshApex(this.refreshTable);
                })
                .catch(error => {
                    window.console.log('Error ====> '+error);
                    this.dispatchEvent(new ShowToastEvent({
                        title: 'Error!!', 
                        message: error.message, 
                        variant: 'error'            }),);
                });
            }
            @wire(CurrentPageReference) wiredPageRef(pageRef) 
                { this.pageRef = pageRef; 
                    if(this.pageRef) 
                        registerListener('sendmessage', this.handleChange, this); }
            connectedCallback() {
                this.DetailProjet= this.pageRef.attributes.recordProjet;
                sessionStorage.setItem('DetailProjet',this.DetailProjet);
    console.log(JSON.stringify( sessionStorage.getItem('DetailProjet')));
                this.tableSprint() ;
                return refreshApex(this.data);
            }
            disconnectedCallback() {
  // unsubscribe from inputChangeEvent event
            unregisterAllListeners(this);
            }
}
