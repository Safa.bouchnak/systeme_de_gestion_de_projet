import { LightningElement, wire, api, track } from 'lwc';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import PROJET_FIELD from '@salesforce/schema/Sprint__c.projet__c';
import NAME_FIELD from '@salesforce/schema/Sprint__c.Name';
import FINDATE_FIELD from '@salesforce/schema/Sprint__c.Date_Fin__c';
import FIRSTDATE_FIELD from '@salesforce/schema/Sprint__c.Date_Debut__c';
import saveSprintRecord from '@salesforce/apex/sprintControler.saveSprintRecord'; 
import DESCRIPTION_FIELD from '@salesforce/schema/Sprint__c.Description__c';
import deleteSprint from '@salesforce/apex/sprintControlerLWC.deleteSprint' ;
import getSprint from '@salesforce/apex/sprintControlerLWC.getSprint' ;
import {refreshApex} from '@salesforce/apex';
import { CurrentPageReference } from 'lightning/navigation';
import { registerListener, unregisterAllListeners } from 'c/pubsub';
import { NavigationMixin } from 'lightning/navigation';
import { getRecord } from 'lightning/uiRecordApi';
import { fireEvent } from 'c/pubsub';
import getProjetWorkTime from '@salesforce/apex/workTimeProjet.getProjetWorkTime'; 
import updateProjetWorkTime from '@salesforce/apex/workTimeProjet.updateProjetWorkTime'; 
const columns = [
    {type: "button-icon", typeAttributes: {  
        iconName: 'action:share',
        label: 'record_Apex',  
        name: 'record_Apex',  
        title: 'record_Apex',  
        disabled: false,  
        value: 'record_Apex',  
        iconPosition: 'right',
    } 

},  
{label: 'Liste Sprint', fieldName: 'Name' },
{label:'Description',fieldName:'Description__c'},
{label:'First Date',fieldName:'Date_Debut__c'},
{label:'Laste Date',fieldName:'Date_Fin__c'},

{type: "button-icon", typeAttributes: {  
    label: 'delete',  
    name: 'delete',  
    title: 'delete',  
    disabled: false,  
    value: 'delete',  
    iconName: 'action:delete',
    iconPosition: 'left'  
}},
{type: "button-icon", typeAttributes: {  
    label: 'Edit',  
    name: 'edit',  
    title: 'edit' , 
    disabled: false,  
    value: 'edit',  
    iconName: 'action:edit',
    iconPosition: 'left'  
}},
{type: "button-icon" , typeAttributes: {  
    label: 'record_details',  
    name: 'record_details',  
    title: 'record_details',  
    disabled: false,  
    value: 'record_details',  
    iconName: 'action:preview',
    iconPosition: 'left'  
    
} },
{type: "button-icon", typeAttributes: {  
    iconName: 'action:share',
    label: 'Add_Commentaire',  
    name: 'Add_Commentaire',  
    title: 'Add_Commentaire',  
    disabled: false,  
    value: 'Add_Commentaire',  
    iconPosition: 'Add_Commentaire' , 
}}];
export default class ListeSprint  extends NavigationMixin(LightningElement){
    @api Sprint__c;
    @api recordId;
    selectedRecords = [];
    refreshTable;  
    timestamp = new Date();
    @track columns = columns;
    @track showpages =false;
    @track bShowModal = false;
    @track recordId;
    @track currentRecordId;
    @track isEditForm = false;
    @track showLoadingSpinner = false;
    @track DetailProjet;
    @track detailSprint;
    @track data;
    @track aya = true;
    @track record = [];
    @track error; 
    @track openmodel = false;
    @track SprintRecord={
        Name : NAME_FIELD,
        Description__c :DESCRIPTION_FIELD,
        projet__c:PROJET_FIELD,
        Date_Debut__c:FIRSTDATE_FIELD,
        Date_Fin__c:FINDATE_FIELD,
    };
@wire(CurrentPageReference) pageRef;
    navigateTolisteuserstory(event) {
        const record = event.detail.row;

        console.log('userstory'+record.Name)
    
    fireEvent(this.pageRef, 'sendmessage',{story: record.Id});
        this[NavigationMixin.Navigate]({
            type: 'standard__navItemPage',
            attributes: {
                apiName: 'Liste_des_user_story',
                recordSprint: record,
                recordP :this.DetailProjet,
            },
        });
    }
    handleNavigateToProjet(event) {
        event.preventDefault();
        this[NavigationMixin.Navigate]({
            type: 'standard__navItemPage',
            attributes: {
                apiName:   'liste_des_projet',
            },
        });
    }
    handleNameuserchange(event){
        this.SprintRecord.Name= event.target.value;
        window.console.log('name==>'+this.SprintRecord.Name);
    }
    handleDescriptionchange(event){
        this.SprintRecord.Description__c= event.target.value;
        window.console.log('Description==>'+this.SprintRecord.Description__c);
    }
    handleDateDebutuserchange(event){
        this.SprintRecord.Date_Debut__c= event.target.value;
        window.console.log('first data ==>'+this.SprintRecord.Date_Debut__c);
    }
    handleDateFinchange(event){
        this.SprintRecord.Date_Fin__c= event.target.value;
        window.console.log('Description==>'+this.SprintRecord.Date_Fin__c);
    }
    openmodal(event) {
        console.log('brabie => ',event);
        this.openmodel = true;
    }
    closeModal() {
        this.openmodel = false;
    } 
    saveMethod() {
console.log('nom projet===>'+JSON.stringify(this.SprintRecord));
saveSprintRecord({SpObjAcc: this.SprintRecord, projObj: this.DetailProjet})
            .then (result => {
                this.SprintRecord ={};
            window.console.log('result===>'+result);
           // show sucess message
            this.dispatchEvent(new ShowToastEvent({
                title:'Succès!',
                message: 'User Story créé avec succès!',
                variant: 'Succès!',
                
            }),);
               // refreshing table data using refresh apex
        return refreshApex(this.refreshTable);
            })
            .catch(error => {
                this.error =error.message;
            });
        this.closeModal();
    }
    handleSuccess() {
        return refreshApex(this.refreshTable);
    }
    handleRowActions(event) {
    let actionName = event.detail.action.name;
    window.console.log('actionName ====> ' + actionName);
    let row = event.detail.row;
    window.console.log('row ====> ' + row);
    // eslint-disable-next-line default-case
    switch (actionName) {
        case 'recordId':
            this.navigateToViewRecordPage(event);
            break;
        case 'record_Apex':
        this.navigateTolisteuserstory(event);
        break;
        case 'record_details':
            this.viewCurrentRecord(row);
            console.log("row!" +JSON.stringify(row));
            break;
        case 'edit':
            this.editCurrentRecord(row);
            break;
        case 'delete':
            this.deleteSprint(row);
            break;
            case 'Add_Commentaire':
                this.showpage(row);
                break;
    }
}

showpage(currentRow){
    this.showpages=true;
    this.idd = currentRow.Id;
    this.record = currentRow;
    console.log('result==>',JSON.stringify( this.DetailProjet));
    getProjetWorkTime({PObj:this.DetailProjet.Id})
    .then (result => {
        console.log('result33==>',this.DetailProjet.Id)
        console.log('result==>',result);
        updateWorkTime({PObj:this.DetailProjet,SumTim: result})
            .then (result => {
                console.log('update===>',result);
            })
})

}
@api refresh() {
    this.timestamp = new Date();
}
 //Navigate to Reports
 navigateToReports() {
    this[NavigationMixin.Navigate]({
        type: 'standard__objectPage',
        attributes: {
            objectApiName: 'Event',
            actionName: 'home'
        },
    });
}
@wire (getSprint,{ projObj:this.DetailProjet}) User_Story__c;
User_Story__c(result)
{
    this.refreshTable = result;
    if (result.data) {
        this.data = result.data;
        this.error = undefined;

    } else if (result.error) {
        this.error = result.error;
        this.data = undefined;
    }
}

    tableSprint() {    
        getSprint({ projObj:this.DetailProjet})
            .then (result => {
                this.refreshTable = result;
            this.SprintRecord ={};
                this.data = result;
            })}
            viewCurrentRecord(currentRow) {
                this.bShowModal = true;
                this.isEditForm = false;
                this.record = currentRow;
            }
            closeModalAction() {
                this.bShowModal = false;
            }
            editCurrentRecord(currentRow) {
                // open modal box
                this.bShowModal = true;
                this.isEditForm = true;
        
                // assign record id to the record edit form
                this.currentRecordId = currentRow.Id;
            }
            editRecord(){
                this.editCurrentRecord(this.safa);
                console.log('safa'+JSON.stringify(this.safa));
            }
            handleSubmit(event) {
                // prevending default type sumbit of record edit form
                event.preventDefault();
        
                // querying the record edit form and submiting fields to form
                this.template.querySelector('lightning-record-edit-form').submit(event.detail.fields);
        
                // closing modal
                this.bShowModal = false;
        
                // showing success message
                this.dispatchEvent(new ShowToastEvent({
                    title: 'Succès!',
                    message: event.detail.fields.Name + 'User Story mis à jour avec succès !!.',
                    variant: 'Succès!'
                }),);
                return refreshApex(this.refreshTable);
            }

    
    
            deleteSprint(currentRow) {
                let currentRecord = [];
                currentRecord.push(currentRow.Id);
                this.showLoadingSpinner = true;
        
                // calling apex class method to delete the selected contact
                deleteSprint({lstSprintIds: currentRecord})
                .then(result => {
                    window.console.log('result ====> ' + result);
                    this.showLoadingSpinner = false;
                    // showing success message
                    this.dispatchEvent(new ShowToastEvent({
                        title: 'Succès!',
                        message: currentRow.Name +'Sprint supprimé.',
                        variant: 'Succès!'
                    }),);
                    // refreshing table data using refresh apex
                    return refreshApex(this.refreshTable);
                })
                .catch(error => {
                    window.console.log('Error ====> '+error);
                    this.dispatchEvent(new ShowToastEvent({
                        title: 'Error!!', 
                        message: error.message, 
                        variant: 'error'            }),);
                });
            }
            @wire(CurrentPageReference) wiredPageRef(pageRef) 
                { this.pageRef = pageRef; 
                    if(this.pageRef) 
                        registerListener('sendmessage', this.handleChange, this); }
            connectedCallback() {
                this.DetailProjet= this.pageRef.attributes.recordProjet;
                this.tableSprint() ;

                return refreshApex(this.data);
            }
            disconnectedCallback() {
  // unsubscribe from inputChangeEvent event
            unregisterAllListeners(this);
            console.log('nom Projet===>'+JSON.stringify(this.DetailProjet.Name));
            console.log('nom Projet===>'+JSON.stringify(this.DetailProjet.Name));
            }
}