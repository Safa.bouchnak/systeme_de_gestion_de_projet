
import { LightningElement, wire, api, track } from 'lwc';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import saveuserstoryRecord from '@salesforce/apex/userstorycontroler.saveuserstoryRecord'; 
import NAME_FIELD from '@salesforce/schema/User_Story__c.Name';
import USER_STORY_OBJECT from '@salesforce/schema/User_Story__c';
import DESCRIPTION_FIELD from '@salesforce/schema/User_Story__c.Description__c';
import TEMPS_FILED from '@salesforce/schema/User_Story__c.Temps_Consome__c';
import ETAT_FIELD from '@salesforce/schema/User_Story__c.Etat__c';
import deleteUserStory from '@salesforce/apex/userstorycontrolerLWC.deleteUserStory' ;
import {refreshApex} from '@salesforce/apex';
import getUserStory from '@salesforce/apex/userstorycontrolerLWC.getUserStory';
import { CurrentPageReference } from 'lightning/navigation';
import { registerListener, unregisterAllListeners } from 'c/pubsub';
import { getPicklistValues, getPicklistValuesByRecordType, getObjectInfo } from 'lightning/uiObjectInfoApi';
import { NavigationMixin } from 'lightning/navigation';
import { getRecord } from 'lightning/uiRecordApi';
import { fireEvent } from 'c/pubsub';

const columns = [
    {type: "button-icon", typeAttributes: {  
        iconName: 'action:share',
        label: 'record_Apex',  
        name: 'record_Apex',  
        title: 'record_Apex',  
        disabled: false,  
        value: 'record_Apex',  
        iconPosition: 'right', 
    } 

},  
{label: 'Liste des User Story', fieldName: 'Name' },
{label:'Temps consomée',fieldName:'Temps_Consome__c'},
{type: "button-icon", typeAttributes: {  
    label: 'delete',  
    name: 'delete',  
    title: 'delete',  
    disabled: false,  
    value: 'delete',  
    iconName: 'action:delete',
    iconPosition: 'left'  
}},
{type: "button-icon", typeAttributes: {  
    label: 'Edit',  
    name: 'edit',  
    title: 'edit' ,
    disabled: false,  
    value: 'edit',  
    iconName: 'action:edit',
    iconPosition: 'left'  
}},
{type: "button-icon" , typeAttributes: {  
    label: 'record_details',  
    name: 'record_details',  
    title: 'record_details',  
    disabled: false,  
    value: 'record_details',  
    iconName: 'action:preview',
    iconPosition: 'left'  
    
} }];
export default class listeUserstory extends NavigationMixin(LightningElement)  {
    @api User_Story__c;
    @api recordId;
    @track inProgressTasks = [];
    @track completedTasks = [];
    @track deferredTasks = [];
    selectedRecords = [];
    refreshTable;  
    @track notStartedTasks = [];
    @track element =[]; 
    @track columns = columns;
    @track bShowModal = false;
    @track recordId;
    @track currentRecordId;
    @track isEditForm = false;
    @track showLoadingSpinner = false;
    @track DetailProjet;
    @track minutes;
    @track timeWork;
    @track data;
    @track aya = true;
    @track record = [];
    @track error; 
    @track detailSprint;
    @track  verifbool=true;
    @track  verifbools=false;
    @track openmodel = false;
    @track value;
    @track UserStoryRecord={
        Name : NAME_FIELD,
        Description__c :DESCRIPTION_FIELD,
        Temps_Consome__c:TEMPS_FILED,
        Etat__c:ETAT_FIELD
    };

    @wire(CurrentPageReference) pageRef;
    navigateTolisteuserstory(event) {
        const record = event.detail.row;
        console.log('userstoryzz',record.Id)
        fireEvent(this.pageRef, 'sendmessage',{story: record.Id});
        this[NavigationMixin.Navigate]({
            type: 'standard__navItemPage',
            attributes: {
                apiName:   'liste_des_tickets',
                recordUserStory: record,
                recordSprint :this.detailSprint,
                recordProjet :this.DetailProjet,
            },
        });
       
    }

 

    
@wire(getPicklistValues, {
    recordTypeId : '012000000000000AAA',
    fieldApiName : ETAT_FIELD,
})
    wiredPickListValue({ data, error }){
        if(data){
            console.log(` Picklist values are `, data. values);
            this.pickListvalues = data.values;
            this.error = undefined;
        }
        if(error){
            console.log(` Error while fetching Picklist values  ${error}`);
            this.error = error;
            this.pickListvalues = undefined;
        }
    }
    @wire(getPicklistValuesByRecordType, {
        recordTypeId : '01I3z000001Egjd',
        objectApiName : USER_STORY_OBJECT
    })
    wiredRecordtypeValues({data, error}){
        if(data){
            console.log(' Picklist Values ', data.picklistFieldValues.Etat__c.values);
            this.pickListvaluesByRecordType = data.picklistFieldValues.Etat__c.values;
        }
        if(error){
            console.log(error);
        }
    }
    @wire(getObjectInfo,{
        objectApiName : USER_STORY_OBJECT
    })
        wiredObject({data, error}){
            if(data){
                console.log(' Object iformation ', data);
                console.table(data);
            }
            if(error){
                console.log(error);
            }
        } 
    handleNameuserchange(event){
        this.UserStoryRecord.Name= event.target.value;
        window.console.log('name==>'+this.UserStoryRecord.Name);
    }
    handleDescriptionchange(event){
        this.UserStoryRecord.Description__c= event.target.value;
        window.console.log('Description==>'+this.UserStoryRecord.Description__c);
    }
    handleetatchange(event){
        this.UserStoryRecord.Etat__c= event.target.value;
        window.console.log('Etat==>'+this.UserStoryRecord.Etat__c);
    }
    openmodal(event) {
        console.log('brabie => ',event);
        this.openmodel = true;
    }
    closeModal() {
        this.openmodel = false;
    } 
    saveMethod() {

console.log('nom projet===>'+JSON.stringify(this.UserStoryRecord));
    saveuserstoryRecord({objAcc: this.UserStoryRecord, SprintObj: this.detailSprint})
            .then (result => {
            this.UserStoryRecord ={};
            window.console.log('result===>'+JSON.stringify( result));
           // show sucess message

        this.data.push(result);
            this.dispatchEvent(new ShowToastEvent({
                title:'Succès!',
                message: 'User Story créé avec succès!',
                variant: 'Succès!',
                
            }),);
               // refreshing table data using refresh apex
            
            })
            .catch(error => {
                this.error =error.message;
            });
        
        this.closeModal();
    }
    handleSuccess() {
        return refreshApex(this.refreshTable);
    }
    handleRowActions(event) {
    let actionName = event.detail.action.name;

    window.console.log('actionName ====> ' + actionName);

    let row = event.detail.row;

    window.console.log('row ====> ' + row);
    // eslint-disable-next-line default-case
    switch (actionName) {

        case 'recordId':
            this.navigateToViewRecordPage(event);
            break;
        case 'record_Apex':
        this.navigateTolisteuserstory(event);
        break;
        case 'record_details':
            this.viewCurrentRecord(row);
            console.log("row!" +JSON.stringify(row));
            break;
        case 'edit':
            this.editCurrentRecord(row);
            break;
        case 'delete':
            this.deleteUserStory(row);
            break;
    }
}
// @wire (getUserStory,{SprintObj:this.detailSprint}) User_Story__c;
// User_Story__c(result)
// {
//     this.refreshTable = result;
//     if (result.data) {
//         this.data = result.data;
//         this.error = undefined;

//     } else if (result.error) {
//         this.error = result.error;
//         this.data = undefined;
//     }
// }
tableUserStory() {    
    getUserStory({SprintObj:this.detailSprint})
        .then (result => {
            this.refreshTable = result;
            this.data = result;
        })}
        
            viewCurrentRecord(currentRow) {
                this.bShowModal = true;
                this.isEditForm = false;
                this.record = currentRow;
            }
            closeModalAction() {
                this.bShowModal = false;
            }
            editCurrentRecord(currentRow) {
                // open modal box
                this.bShowModal = true;
                this.isEditForm = true;
                // assign record id to the record edit form
                this.currentRecordId = currentRow.Id;
                console.log('data'+JSON.stringify(this.currentRecordId));
            }
      
            handleSubmit(event) {
                // prevending default type sumbit of record edit form
                event.preventDefault();
                // querying the record edit form and submiting fields to form
                this.template.querySelector('lightning-record-edit-form').submit(event.detail.fields);
        
                // closing modal
                this.bShowModal = false;
        
                // showing success message
                this.dispatchEvent(new ShowToastEvent({
                    title: 'Succès!',
                    message: event.detail.fields.Name + 'User Story mis à jour avec succès !!.',
                    variant: 'Succès!'
                }),);
                return refreshApex(this.refreshTable);
            }
            deleteUserStory(currentRow) {
                let currentRecord = [];
                currentRecord.push(currentRow.Id);
                this.showLoadingSpinner = true;
        
                // calling apex class method to delete the selected contact
                deleteUserStory({lstUserStoryIds: currentRecord})
                .then(result => {
                    window.console.log('result ====> ' + result);
        
                    this.showLoadingSpinner = false;
        
                    // showing success message
                    this.dispatchEvent(new ShowToastEvent({
                        title: 'Succès!',
                        message: currentRow.Name +' User Story supprimé.',
                        variant: 'Succès!'
                    }),);
                    // refreshing table data using refresh apex
                    return refreshApex(this.refreshTable);
                })
                .catch(error => {
                    window.console.log('Error ====> '+error);
                    this.dispatchEvent(new ShowToastEvent({
                        title: 'Error!!', 
                        message: error.message, 
                        variant: 'error'            }),);
                });
            }
            gettable(){
            getUserStory({ SprintObj:this.detailSprint})
            .then (data => {
                console.log('data'+JSON.stringify(data));
                this.verifbools=true;
                
                //Seems repetitive, is there a better way to do this?
                if (data) {


                    data.forEach(element => {
                        if (element.Etat__c === 'Not Started') {
                            this.notStartedTasks.push(element);
                            console.log('Not Started'+JSON.stringify(this.notStartedTasks));
                            console.log('Not Started'+JSON.stringify(element));
                        }
                        else if (element.Etat__c === 'In Progress') {
                            this.inProgressTasks.push(element);
                            console.log('In Progress'+JSON.stringify(this.inProgressTasks));
                            console.log('In Progress'+JSON.stringify(element));
                        }
                        else if (element.Etat__c === 'Completed') {
                            this.completedTasks.push(element);
                            console.log('Completed'+JSON.stringify(this.completedTasks));
                        }
                        else if (element.Etat__c === 'Deferred') {
                            this.deferredTasks.push(element);
                            console.log('Deferred'+JSON.stringify(this.deferredTasks));
                            }
                        })
                        this.element=data;
                        console.log('element'+JSON.stringify(this.element));
                    }
                    else if (error) {
                        console.log(error);
                    }
                        
                    })}
            
                    editRecord(){
                    editCurrentRecords(this.element.Id);
                        console.log('data'+(this.element.Id));
                    }
                    save() {
                        let taskToSave = Object.assign({}, this.element );
                        saveuserstoryRecord({objAcc:[taskToSave] , SprintObj: this.detailSprint})
                            .then(() => {
                                console.log('Success!');
                            })
                            .catch(() => {
                                console.log('Something went wrong...');
                            })
                            
                    }
        
                    @wire(CurrentPageReference) wiredPageRef(pageRef) 
                    {this.pageRef = pageRef; 
                        if(this.pageRef) 
                            registerListener('sendmessage', this.handleChange, this); }
                connectedCallback() {
                    var Sprint = this.pageRef.attributes.recordSprint
                    if (Sprint!= null && Sprint!= undefined)
                            {
                        sessionStorage.setItem('detailSprint',JSON.stringify((Sprint)));
                            }
                    this.detailSprint=JSON.parse(sessionStorage.getItem('detailSprint'));
                    console.log(sessionStorage.getItem('detailSprint'));
                    
                    var project= this.pageRef.attributes.recordProjet;
                    if (project!= null && project!= undefined )
                            {
                    sessionStorage.setItem('DetailProjet',JSON.stringify((project)));
                            }
                    this.DetailProjet=JSON.parse(sessionStorage.getItem('DetailProjet'));
                    console.log(sessionStorage.getItem('DetailProjet'));
                    console.log('projet'+JSON.stringify(this.DetailProjet));
                this.gettable();
                this.tableUserStory();
            
            }
            disconnectedCallback() {
            // unsubscribe from inputChangeEvent event
            unregisterAllListeners(this);
            console.log('nom Projet===>'+JSON.stringify(this.DetailProjet.Name));
            console.log('nom Projet===>'+JSON.stringify(this.detailSprint.Name));
            }
        }