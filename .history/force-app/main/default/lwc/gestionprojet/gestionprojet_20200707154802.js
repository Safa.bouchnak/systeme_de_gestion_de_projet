import { LightningElement } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';


export default class gestionprojet extends NavigationMixin(LightningElement) {


    navigateTolisteprojet() {
        // Opens the Account record modal
        // to view a particular record.
        this[NavigationMixin.Navigate]({
            type: 'standard__navItemPage',
            attributes: {
                apiName:   'liste_des_projet',
                
            }
        });
    }

    NavigateToCalender(){
        this[NavigationMixin.Navigate]({
            type: 'standard__objectPage',
            attributes: {
                objectApiName: 'Event',
                actionName: 'home'
            },
        });

    }
    NavigateToResource(){
        this[NavigationMixin.Navigate]({
            "type": "standard__objectPage",
            "attributes": {
                "objectApiName": "Resource__c",
                "actionName": "home"
            }
        });

    }
    NavigateToDashbord(){
        this[NavigationMixin.Navigate]({
            "type": "standard__navItemPage",
            "attributes": {
                "objectApiName": "Dashbord",
                "actionName": "home"
            }
        });
    }

}