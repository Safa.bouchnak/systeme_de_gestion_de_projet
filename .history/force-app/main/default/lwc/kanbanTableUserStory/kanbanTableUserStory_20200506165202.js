import { LightningElement, api, wire, track } from 'lwc';
import getKanbanUserStory from '@salesforce/apex/kanbanTableUserStory.getKanbanUserStory';

export default class kanbanTableUserStory extends LightningElement {
    //Allows the component to be aware of the ID of the record (i.e. the Account)
    //that it is embedded on
    @api recordId;

    @track notStartedTasks = [];
    @track inProgressTasks = [];
    @track completedTasks = [];
    @track deferredTasks = [];
    
    @wire(getKanbanUserStory, { USObj: '$recordId' })
    processTasks({ error, data }) {
        //Seems repetitive, is there a better way to do this?
        if (data) {
            data.forEach(element => {
                if (element.Etat__c === 'Not Started') {
                    this.notStartedTasks.push(element);
                }
                else if (element.Etat__c === 'In Progress') {
                    this.inProgressTasks.push(element);
                }
                else if (element.Etat__c === 'Completed') {
                    this.completedTasks.push(element);
                }
                else if (element.Etat__c === 'Deferred') {
                    this.deferredTasks.push(element);
                }
            });
            this.record = data;
        } else if (error) {
            console.log(error);
        }
    }