import { LightningElement,track} from 'lwc';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import saveprojet from '@salesforce/apex/projetcontrolerLWC.saveprojetRecord'; 
// importing Projet filed
import NAME_FIELD from '@salesforce/schema/Projet__c.Name';
import CATEGORIE_FIELD from '@salesforce/schema/Projet__c.Gat_gorie__c';
import ETAT_FIELD from '@salesforce/schema/Projet__c.Etat__c';
import DATEDEBUT_FILED from '@salesforce/schema/Projet__c.Date_Debut__c';
import DATEFIN_FILED from '@salesforce/schema/Projet__c.Date_fin__c';
import  PRIORITE_FILED from '@salesforce/schema/Projet__c.Priorit__c';
import TYPE_FILED from '@salesforce/schema/Projet__c.Type__c';


export default class Ajouterproj extends LightningElement {
    @track error;   
    @track ProjetRecord={
        Nom : NAME_FIELD,
        Etat :ETAT_FIELD,
        Type :TYPE_FILED,
        Categories : CATEGORIE_FIELD,
        prioriter : PRIORITE_FILED ,
        Date_Debut : DATEDEBUT_FILED,
        Date_fin : DATEFIN_FILED,
    };
    handleNamechchange(event){
        this.ProjetRecord.Nom= event.target.value;
        window.console.log('name==>'+this.ProjetRecord.Nom);
    }
    handleetatchchange(event){
        this.ProjetRecord.Etat= event.target.value;
        window.console.log('Etat==>'+this.ProjetRecord.Etat);
    }
    handleTypehchange(event){
        this.ProjetRecord.Type= event.target.value;
        window.console.log(' Type==>'+this.ProjetRecord.Type);
    }
    handleCategorieschange(event){
        this.ProjetRecord.Categories= event.target.value;
        window.console.log(' Categories==>'+this.ProjetRecord.Categories);
    }
    handleprioriterchange(event){
        this.ProjetRecord.prioriter= event.target.value;
        window.console.log('Prioriter==>'+this.ProjetRecord.prioriter);
    }
    handleDateDebutchange(event){
        this.ProjetRecord.Date_Debut= event.target.value;
        window.console.log('Date_Debut==>'+this.ProjetRecord.Date_Debut);
    }
    handleDatefinchange(event){
        this.ProjetRecord.Date_fin= event.target.value;
        window.console.log('Date_fin==>'+this.ProjetRecord.Date_fin);
    }

handelsave() { 
    saveprojet({objAcc: this.ProjetRecord})
    .then (result => {
    this.ProjetRecord ={};
    window.console.log('result===>'+result);
   // show sucess message
    this.dispatchEvent(new ShowToastEvent({
        title:'success!',
        message: 'Projet Created Sucessfully!',
        variant: 'success!',
    }),);
    })
    .catch(error => {
        this.error =error.message;
    });
}}

