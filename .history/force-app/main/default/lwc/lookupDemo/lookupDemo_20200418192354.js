import { LightningElement, track } from 'lwc';

export default class LookupDemo extends LightningElement {
    @track
   Projet__c;
  
        handleSelected(event) {
            var objectname = event.detail.ObjectName;
            if( objectname === "Projet__c") {
                this. Projet__c = {Name : event.detail.Name, Id: event.detail.Id};
            }
          
        }
}