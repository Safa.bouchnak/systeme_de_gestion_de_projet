import { LightningElement, wire, track ,api } from 'lwc';
import getResults from '@salesforce/apex/RessourceSelect.getResults';
import { NavigationMixin } from 'lightning/navigation';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import findProjet from '@salesforce/apex/searchProject.findProjet';
// importing Projet filed
import NAME_FIELD from '@salesforce/schema/Projet__c.Name';
import CATEGORIE_FIELD from '@salesforce/schema/Projet__c.Gat_gorie__c'
import ETAT_FIELD from '@salesforce/schema/Projet__c.Etat__c';
import DATEDEBUT_FILED from '@salesforce/schema/Projet__c.Date_Debut__c';
import DATEFIN_FILED from '@salesforce/schema/Projet__c.Date_fin__c';
import PRIORITE_FILED from '@salesforce/schema/Projet__c.Priorit__c';
import TYPE_FILED from '@salesforce/schema/Projet__c.Type__c';
import Projet_OBJECT from '@salesforce/schema/Projet__c';
//import pickliste values 
import { getPicklistValues, getPicklistValuesByRecordType, getObjectInfo } from 'lightning/uiObjectInfoApi';
 import deleteProject from '@salesforce/apex/ProjectControllerinsert.deleteProject';
import getProjets from '@salesforce/apex/ProjectControllerinsert.getProjets';
import Ressourcesubmit from '@salesforce/apex/submitResource.Ressourcesubmit';

import saveprojetRecord from '@salesforce/apex/projetcontrolerLWC.saveprojetRecord'; 
import getProjetWorkTime from '@salesforce/apex/workTimeProjet.getProjetWorkTime'; 
import updateProjetWorkTime from '@salesforce/apex/workTimeProjet.updateProjetWorkTime'; 
import getProjetAvencemet from '@salesforce/apex/Avencement.getProjetAvencemet'; 
import updateProjetAvencement from '@salesforce/apex/Avencement.updateProjetAvencement'; 
import getProjetIcon from '@salesforce/apex/IconProjet.getProjetIcon'; 
import updateProjetIcon from '@salesforce/apex/IconProjet.updateProjetIcon'; 
import delayprojet from '@salesforce/apex/delay.delayprojet'; 
import updateProjetdelay from '@salesforce/apex/delay.updateProjetdelay'; 
// importing to refresh the apex if any record changes the datas
import {refreshApex} from '@salesforce/apex';
import {CurrentPageReference} from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';
// datatable columns with row actions
const columnsSearch = [{
	type: "button-icon",
	typestyle: {},
	typeAttributes: {
		iconName: 'action:share',
		label: 'record_Apex',
		name: 'record_Apex',
		title: 'record_Apex',
		disabled: false,
		value: 'record_Apex',
		iconPosition: 'right',
		background: 'darkred',
	}
}, {
	label: 'Project Name',
	fieldName: 'Name'
},
{
	label: 'Project Name',
	fieldName: 'Re'
},
 {
	label: 'Priority',
	fieldName: 'Priorit__c'
}, 
{
	label: 'Start date',
	fieldName: 'Date_Debut__c'
},
 {
	label: 'End date',
	fieldName: 'Date_fin__c'
}, {
	label: 'Time Consumed',
	fieldName: 'Temps_Consome__c'
}, {
	label: 'Advancement',
	fieldName: 'Avencment__c'
}, {
	label: 'state of progress',
	fieldName: 'icone__c'
}, {
	label: 'time limit',
	fieldName: 'delay__c'
}, {
	type: "button-icon",
	typeAttributes: {
		label: 'delete',
		name: 'delete',
		title: 'delete',
		disabled: false,
		value: 'delete',
		iconName: 'action:delete',
		iconPosition: 'left'
	}
}, {
	type: "button-icon",
	typeAttributes: {
		label: 'Edit',
		name: 'edit',
		title: 'edit',
		disabled: false,
		value: 'edit',
		iconName: 'action:edit',
		iconPosition: 'left'
	}
}, {
	type: "button-icon",
	typeAttributes: {
		label: 'record_details',
		name: 'record_details',
		title: 'record_details',
		disabled: false,
		value: 'record_details',
		iconName: 'action:preview',
		iconPosition: 'left'
	
	}
}];
const columns = [{
	type: "button-icon",
	typeAttributes: {
		iconName: 'action:share',
		label: 'record_Apex',
		name: 'record_Apex',
		title: 'record_Apex',
		disabled: false,
		value: 'record_Apex',
		iconPosition: 'right',
	}
}, {
	label: 'Project Name',
	fieldName: 'Name'
}, {
	label: 'Category',
	fieldName: 'Gat_gorie__c'
}, {
	label: 'state',
	fieldName: 'Etat__c'
}, {
	label: 'Type',
	fieldName: 'Type__c'
}, {
	label: 'Priority',
	fieldName: 'Priorit__c'
}, {
	label: 'Start date',
	fieldName: 'Date_Debut__c'
}, 
{
	label: 'End date',
	fieldName: 'Date_fin__c'
}, {
	label: 'Time Consumed',
	fieldName: 'Temps_Consome__c'
}, {
	label: 'Advancement',
	fieldName: 'Avencment__c'
}, {
	label: 'state of progress',
	fieldName: 'icone__c'
}, {
	label: 'time limit',
	fieldName: 'delay__c'
}, 
{
	type: "button-icon",
	typeAttributes: {
		label: 'Add Resource',
		name: 'Add Resource',
		title: 'Add Resource',
		disabled: false,
		value: 'Add Resource',
		iconName: 'action:new_group',
		iconPosition: 'left'
	}

},
{
	type: "button-icon",
	typeAttributes: {
		label: 'delete',
		name: 'delete',
		title: 'delete',
		disabled: false,
		value: 'delete',
		iconName: 'action:delete',
		iconPosition: 'left'
	}
}, {
	type: "button-icon",
	typeAttributes: {
		label: 'Edit',
		name: 'edit',
		title: 'edit',
		disabled: false,
		value: 'edit',
		iconName: 'action:edit',
		iconPosition: 'left'
	}
}, {
	type: "button-icon",
	typeAttributes: {
		label: 'record_details',
		name: 'record_details',
		title: 'record_details',
		disabled: false,
		value: 'record_details',
		iconName: 'action:preview',
		iconPosition: 'left'
	}
}];
export
default class listeprojet extends NavigationMixin(LightningElement) {@
	wire(CurrentPageReference) pageRef;
	_errors;
	selectedRecords = [];
	refreshTable;
	error;
	@track Name = "";
	@api objectName = 'Resource__c';
@api fieldName = 'Name';
@api Label;
@track searchRecords = [];
@track selectedRecords = [];
	@api required = false;
	@api LoadingText = false;
	@track txtclassname = 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
	@track messageFlag = false;
	@track Gat_gorie__c = "";
	@track Etat__c = "";
	@track viewDetails = false;
	@track pickListvalues;
	@track values;
	@track Etat__c;
	@track Type__c;@
	track Priorit__c;@
	track error;@
	track pickListvaluesByRecordType;@
	track data;@
	track columns = columns;@
	track columnsSearch = columnsSearch;@
	track ProjectLists;@
	track record = [];@
	track bShowModal = false;@
	track recordId;@
	track currentRecordId;@
	track isEditForm = false;@
	track showLoadingSpinner = false;@
	track data = [];@
	track dataserch = [];@
	track tableLoadingState = true;@
	track tableDisp = false;@
	track Avencment__c = '';@
	track delay__c = '';@
	track icone__c = '';@
	track Temps_Consome__c = '';
	@track ProjetResource;
	@track ProjetRecord = {
		Name: NAME_FIELD,
		Type__c: TYPE_FILED,
		Etat__c: ETAT_FIELD,
		Gat_gorie__c: CATEGORIE_FIELD,
		Priorit__c: PRIORITE_FILED,
		Date_fin__c: DATEFIN_FILED,
		Date_Debut__c: DATEDEBUT_FILED,
	}
	get options() {
		return [{
			label: 'Nouveau',
			value: 'Nouveau'
		}, {
			label: 'En Cours',
			value: 'En Cours'
		}, {
			label: 'Terminé',
			value: 'Terminé'
		}, {
			label: 'Annulé',
			value: 'Annulé'
		}, ];
	}
	handleCheckboxChange(event) {
		this.viewDetails = event.target.checked;
	}
	handleNameserchChange(event) {
		this.Name = event.detail.value;
	}
	handleGat_gorie__cChange(event) {
		this.Gat_gorie__c = event.detail.value;
	}
	handleEtat__cChange(event) {
		this.Etat__c = event.detail.value;
	}
	handleSearch() {
		let params = {};
		params.Name = this.Name;
		params.Gat_gorie = this.Gat_gorie__c;
		params.Etat = this.Etat__c;
		findProjet(params).then(ProjectList => {
			true;
			this.ProjectLists = ProjectList;
		}).catch(error => {
			this.error = error;
		});
		this.tableDisp = true;
	}@
	wire(CurrentPageReference) pageRef;
	navigateTolisteuserstory(event) {
		const record = event.detail.row;
		// sessionStorage.setItem('SessionRecord',JSON.stringify(record));
		fireEvent(this.pageRef, 'sendmessage', {
			safa: record.Id
		});
		this[NavigationMixin.Navigate]({
			type: 'standard__navItemPage',
			attributes: {
				apiName: 'Sprint_List',
				recordProjet: record,
			},
		});
	}
	// Navigate to New Record Page
	navigateToViewRecordPage(event) {
		const record = event.detail.row;
		console.log("navigation !" + record.Id);
		this[NavigationMixin.Navigate]({
			type: 'standard__recordPage',
			attributes: {
				"recordId": record.Id,
				"objecctApiName": "Projet__c",
				"actionName": "view"
			},
		});
	}
	handleNameserchChange(event) {
		this.Name = event.detail.value;
	}
	handleGat_gorie__cChange(event) {
		this.Gat_gorie__c = event.detail.value;
	}
	handleEtat__cChange(event) {
		this.Etat__c = event.detail.value;
	}
	handleSearch() {
		let params = {};
		params.Name = this.Name;
		params.Gat_gorie = this.Gat_gorie__c;
		params.Etat = this.Etat__c;
		findProjet(params).then(ProjectList => {
			true;
			this.ProjectLists = ProjectList;
			console.log("row!" + JSON.stringify(this.ProjectLists));
			console.log("row2" + JSON.stringify(dataserch));
			console.log("row2" + JSON.stringify(dataserch));
		}).catch(error => {
			this.error = error;
		});
		this.tableDisp = true;
	}
	// navigate to user story
	@
	wire(CurrentPageReference) pageRef;
	navigateTolisteuserstory(event) {
		const record = event.detail.row;
		fireEvent(this.pageRef, 'sendmessage', {
			safa: record.Id
		});
		this[NavigationMixin.Navigate]({
			type: 'standard__navItemPage',
			attributes: {
				apiName: 'Sprint_List',
				recordProjet: record
			},
		});
	}
	// Navigate to New Record Page
	navigateToViewRecordPage(event) {
		const record = event.detail.row;
		console.log("navigation !" + record.Id);
		this[NavigationMixin.Navigate]({
			type: 'standard__recordPage',
			attributes: {
				"recordId": record.Id,
				"objecctApiName": "Projet__c",
				"actionName": "view"
			},
		});
	}
	handleRowActions(event) {
		let actionName = event.detail.action.name;
		let row = event.detail.row;
		// eslint-disable-next-line default-case
		switch (actionName) {
			case 'Add Resource':
			this.AddResource(event);
			break;
		case 'recordId':
			this.navigateToViewRecordPage(event);
			break;
		case 'record_Apex':
			this.navigateTolisteuserstory(event);
			break;
		case 'record_details':
			this.viewCurrentRecord(row);
			console.log("row!" + JSON.stringify(row));
			break;
		case 'edit':
			this.editCurrentRecord(row);
			break;
		case 'delete':
			this.deleteProject(row);
			break;
		}
	}@
	wire(getPicklistValues, {
		recordTypeId: '012000000000000AAA',
		fieldApiName: ETAT_FIELD,
	})
	wiredPickListValue({
		data, error
	}) {
		if (data) {
			this.pickListvalues = data.values;
			this.error = undefined;
		}
		if (error) {
			this.error = error;
			this.pickListvalues = undefined;
		}
	}@
	wire(getPicklistValuesByRecordType, {
		recordTypeId: '0123z000000KkUo',
		objectApiName: Projet_OBJECT
	})
	wiredRecordtypeValues({
		data, error
	}) {
		if (data) {
			this.pickListvaluesByRecordType = data.picklistFieldValues.Priorit__c.values;
			this.Type__c = data.picklistFieldValues.Type__c.values;
		}
		if (error) {
			console.log(error);
		}
	}@
	wire(getObjectInfo, {
		objectApiName: Projet_OBJECT
	})
	wiredObject({
		data, error
	}) {
		if (data) {
			console.table(data);
		}
		if (error) {
			console.log(error);
		}
	}
	handleNamechange(event) {
		this.ProjetRecord.Name = event.target.value;
	}
	handleTypechange(event) {
		this.ProjetRecord.Type__c = event.target.value;
	}
	handleetatchange(event) {
		this.ProjetRecord.Etat__c = event.target.value;
	}
	handlecategoriechange(event) {
		this.ProjetRecord.Gat_gorie__c = event.target.value;
	}
	handlePrioritechange(event) {
		this.ProjetRecord.Priorit__c = event.target.value;
	}
	handleDatefinchange(event) {
		this.ProjetRecord.Date_fin__c = event.target.value;
	}
	handleDateDebutchange(event) {
		this.ProjetRecord.Date_Debut__c = event.target.value;
	}@
	track openmodel1 = false;
	openOrCloseModal() {
		this.openmodel1 = !this.openmodel1;
	}
	openmodal1() {
		this.openmodel1 = true;
	}
	closeModal1() {
		this.openmodel1 = false;
	}
	saveMethodProjet() {
		saveprojetRecord({
			objAcc: this.ProjetRecord
		}).then(result => {
			// show sucess message
			this.dispatchEvent(new ShowToastEvent({
				title: 'Success!',
				message: 'Projet créé avec succès!',
				variant: 'success',
			}), );
			// refreshing table data using refresh apex
			return refreshApex(this.refreshTable);
		}).catch(error => {
			this.error = error.message;
		});
		this.closeModal1();
	}
	handleSuccess() {
		return refreshApex(this.refreshTable);
	}
	// Getting Contacts using Wire Service
	@
	wire(getProjets)
	Projet__c(result) {
		this.refreshTable = result;
		if (result.data) {
			this.data = result.data;
			this.error = undefined;
		} else if (result.error) {
			this.error = result.error;
			this.data = undefined;
		}
	}
	handleSuccess() {
		return refreshApex(this.refreshTable);
	}
	// view the current record details
	viewCurrentRecord(currentRow) {
		this.bShowModal = true;
		this.isEditForm = false;
		this.record = currentRow;
		getProjetAvencemet({
			ProjetID: this.record.Id
		}).then(result => {
			console.log('avencement' + result)
			updateProjetAvencement({
				PObj: this.record,
				SumUserCompleted: result
			})
		})
		getProjetWorkTime({
			ProjetID: this.record.Id
		}).then(result => {

			console.log('time' + result)
			updateProjetWorkTime({
				PObj: this.record,
				SumTim: result
			})
		})
		getProjetIcon({
			ProjetID: this.record.Id
		}).then(result => {
			console.log('Icon' + result)
			updateProjetIcon({
				PObj: this.record,
				icon: result
			})
		})
		delayprojet({
			ProjetID: this.record.Id
		}).then(result => {
			console.log('Delay' + result)
			updateProjetdelay({
				PObj: this.record,
				delay: result
			})
		})
	}
	// closing modal box
	closeModal() {
		this.bShowModal = false;
	}
	editCurrentRecord(currentRow) {
		// open modal box
		this.bShowModal = true;
		this.isEditForm = true;
		// assign record id to the record edit form
		this.currentRecordId = currentRow.Id;
	}
	deleteProject(currentRow) {
		let currentRecord = [];
		currentRecord.push(currentRow.Id);
		this.showLoadingSpinner = true;

		// calling apex class method to delete the selected contact
		deleteProject({lstProjectIds: currentRecord})
		.then(result => {
			window.console.log('result ====> ' + result);

			this.showLoadingSpinner
			
			
			= false;

			// showing success message
			this.dispatchEvent(new ShowToastEvent({
				title: 'Succès!',
				message: currentRow.Name +' Project supprimé.',
				variant: 'success',
			}),);
			// refreshing table data using refresh apex
			return refreshApex(this.refreshTable);
		})
		.catch(error => {
			window.console.log('Error ====> '+error);
			this.dispatchEvent(new ShowToastEvent({
				title: 'Error!!', 
				message: error.message, 
				variant: 'error'            }),);
		});
	}
	
	// handleing record edit form submit
	handleSubmit(event) {
		// prevending default type sumbit of record edit form
		event.preventDefault();
		// querying the record edit form and submiting fields to form
		this.template.querySelector('lightning-record-edit-form').submit(event.detail.fields);
		// closing modal
		this.bShowModal = false;
		// showing success message
		this.dispatchEvent(new ShowToastEvent({
			title: 'Success!',
			message: 'Project successfully updated !!.',
		variant: 'success',
		}), );
	}
	// refreshing the datatable after record edit form success
	handleSuccess() {
		return refreshApex(this.refreshTable);
	}
	//delet projet

			
	@track openmodelResource = false;
    AddResource(event) {
		this.ProjetResource= event.detail.row;
		console.log('Projet==>'+JSON.stringify(this.ProjetResource));
		this.openmodelResource = true
		
    }
    closeModalResource() {
        this.openmodelResource = false
    } 
    AddTeam() {
		
	console.log('resource===>'+JSON.stringify(this.selectedRecords));
	console.log('Projet==>'+JSON.stringify(this.ProjetResource));
	Ressourcesubmit({Resources:this.selectedRecords ,proj:this.ProjetResource.Id})
        .then (result => {
            window.console.log('result===>'+JSON.stringify(result));
        // show sucess message
        this.dispatchEvent(new ShowToastEvent({
            title:'Succès!',
            message: 'Resource Affected success!',
            variant: 'success',
		}),)})
        .catch(error => {
            this.error =error.message;
        });
        this.closeModalResource();
	
}
	searchField(event) {
        var currentText = event.target.value;
        var selectRecId = [];
        for(let i = 0; i < this.selectedRecords.length; i++){
            selectRecId.push(this.selectedRecords[i].recId);
        }
        this.LoadingText = true;
        getResults({ ObjectName: this.objectName, fieldName: this.fieldName, value: currentText, selectedRecId : selectRecId })
        .then(result => {
            this.searchRecords= result;
            this.LoadingText = false;
            this.txtclassname =  result.length > 0 ? 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click slds-is-open' : 'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
            if(currentText.length > 0 && result.length == 0) {
				this.messageFlag = true;
				console.log('lookup'+this.txtclassname)
            }
            else {
                this.messageFlag = false;
            }

            if(this.selectRecordId != null && this.selectRecordId.length > 0) {
                this.iconFlag = false;
                this.clearIconFlag = true;
            }
            else {
                this.iconFlag = true;
                this.clearIconFlag = false;
            }
        })
        .catch(error => {
            console.log('-------error-------------'+error);
            console.log(error);
        });
        
    }
    
	setSelectedRecord(event) {
        var recId = event.currentTarget.dataset.id;
        var selectName = event.currentTarget.dataset.name;
        let newsObject = { 'recId' : recId ,'recName' : selectName };
        this.selectedRecords.push(newsObject);
        this.txtclassname =  'slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click';
        let selRecords = this.selectedRecords;
		this.template.querySelectorAll('lightning-input').forEach(each => {
            each.value = '';
        });
        const selectedEvent = new CustomEvent('selected', { detail: {selRecords}, });
        // Dispatches the event.
        this.dispatchEvent(selectedEvent);
    }

    removeRecord (event){
        let selectRecId = [];
        for(let i = 0; i < this.selectedRecords.length; i++){
            if(event.detail.name !== this.selectedRecords[i].recId)
                selectRecId.push(this.selectedRecords[i]);
        }
        this.selectedRecords = [...selectRecId];
        let selRecords = this.selectedRecords;
        const selectedEvent = new CustomEvent('selected', { detail: {selRecords}, });
        // Dispatches the event.
        this.dispatchEvent(selectedEvent);
    }
}
