import { LightningElement, api, wire, track } from 'lwc';
import getTicket from '@salesforce/apex/TaskbodControler.getTicket';

export default class TaskBoard extends LightningElement {
    //Allows the component to be aware of the ID of the record (i.e. the Account)
    //that it is embedded on
    @api recordId;

    @track notStartedTasks = [];
    @track inProgressTasks = [];
    @track completedTasks = [];
    @track deferredTasks = [];
    
    @wire( getTicket, { recordId: '$recordId' })
    processTasks({ error, data }) {
        //Seems repetitive, is there a better way to do this?
        if (data) {
            data.forEach(Ticket__c => {
                if (Ticket__c.tat__c === 'Not Started') {
                    this.notStartedTasks.push(Ticket__c);
                }
                else if (Ticket__c.tat__c === 'In Progress') {
                    this.inProgressTasks.push(Ticket__c);
                }
                else if (Ticket__c.tat__c === 'Completed') {
                    this.completedTasks.push(Ticket__c);
                }
    
                else if (Ticket__c.tat__c === 'Deferred') {
                    this.deferredTasks.push(Ticket__c);
                }
            });
            this.record = data;
        } else if (error) {
            console.log(error);
        }
    }
}