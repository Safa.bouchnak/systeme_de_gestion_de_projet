import { LightningElement ,wire, track,api } from 'lwc';

import { CurrentPageReference } from 'lightning/navigation';
import { registerListener, unregisterAllListeners } from 'c/pubsub';
import NAME_FIELD from '@salesforce/schema/Sous_Ticket__c.Name';
import DESCRIPTION_FIELD from '@salesforce/schema/Sous_Ticket__c.Description__c';
import ETAT_FIELD from '@salesforce/schema/Sous_Ticket__c.tat__c';
import SOUS_TICKET_OBJECT from '@salesforce/schema/Sous_Ticket__c';
import { NavigationMixin } from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';
import { getPicklistValues, getPicklistValuesByRecordType, getObjectInfo } from 'lightning/uiObjectInfoApi';
import getSousTicket from '@salesforce/apex/sousTicketControler.getSousTicket';
import deleteSousTicket from '@salesforce/apex/sousTicketControler.deleteSousTicket';
import sousTicketupdateEtat from '@salesforce/apex/sousTicketUpdate.sousTicketupdateEtat';
import TICKET_FIELD from '@salesforce/schema/Sous_Ticket__c.Ticket__c';
import NOTES_FIELD from '@salesforce/schema/Sous_Ticket__c.Notes__c';
import updateNotesSousTicket from '@salesforce/apex/updateNoteSousTicket.updateNotesSousTicket';
import saveSousTicketRecord from '@salesforce/apex/sousTicketControlerLWC.saveSousTicketRecord';

import getUserWorkTime from '@salesforce/apex/workTimeUserStory.getUserWorkTime';
import updateUserWorkTime from '@salesforce/apex/workTimeUserStory.updateUserWorkTime';

import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import {refreshApex} from '@salesforce/apex';
const columns = [
 
    {label: 'List of Under Ticket', fieldName: 'Name' },
    {label: 'State', fieldName: 'tat__c' },
    {label: 'Description', fieldName: 'Description__c' },

{type: "button-icon", typeAttributes: {  
    label: 'delete',  
    name: 'delete',  
    title: 'delete',  
    disabled: false,  
    value: 'delete',  
    iconName: 'action:delete',
    iconPosition: 'left'  
}},
{type: "button-icon", typeAttributes: {  
    label: 'Edit',  
    name: 'edit',  
    title: 'edit' , 
    disabled: false,  
    value: 'edit',  
    iconName: 'action:edit',
    iconPosition: 'left'  
}},
{type: "button-icon" , typeAttributes: {  
    label: 'record_details',  
    name: 'record_details',  
    title: 'record_details',  
    disabled: false,  
    value: 'record_details',  
    iconName: 'action:preview',
    iconPosition: 'left'  
    
} },
{type: "button-icon", typeAttributes: {  
    iconName: 'action:share',
    label: 'Add_Commentaire',  
    name: 'Add_Commentaire',  
    title: 'Add_Commentaire',  
    disabled: false,  
    value: 'Add_Commentaire',  
    iconPosition: 'Add_Commentaire' , 
}} ];

export default class ListeDesSousTache extends NavigationMixin(LightningElement){
selectedRecords = [];
refreshTable;
Etat;
timestamp = new Date();
@track Icon_Completed=false;
@track Icon_Deferred=false;
@track Icon_Not_Started =false;
@track Icon_In_Progress =false;
@track detail;
@track showpages =false;
@track bShowModal = false;
@track recordId;
@track verification;
@track isEditForm = false;
@track showLoadingSpinner = false;
@track data;
@track note;
@track record = [];
@track error; 
@track openmodel = false;
@track pickListvalues;
@track columns = columns;
@track DetailTicket;
@track DetailUserStory;
@track DetailProjet;
@track values;
@track currentRecordId;
@track idd;
@track tat__c;
@track Notes__c;
@track today;
@track pickListvaluesByRecordType;
@track displayIconName ;
@track NoteRecord={
    Notes__c:NOTES_FIELD
}

@track SousTicketRecord={
    Name : NAME_FIELD,
    Description__c :DESCRIPTION_FIELD,
    tat__c:ETAT_FIELD,
    Notes__c:NOTES_FIELD,
    Ticket__c:TICKET_FIELD,
}


@wire(getPicklistValues, {
    recordTypeId : '012000000000000AAA',
    fieldApiName : ETAT_FIELD,
})
    wiredPickListValue({ data, error }){
        if(data){
            console.log(` Picklist values are `, data. values);
            this.pickListvalues = data.values;
            this.error = undefined;
        }
        if(error){
            console.log(` Error while fetching Picklist values  ${error}`);
            this.error = error;
            this.pickListvalues = undefined;
        }
    }
    @wire(getPicklistValuesByRecordType, {
        recordTypeId : '01I3z000001EgoF',
        objectApiName : SOUS_TICKET_OBJECT
    })
    wiredRecordtypeValues({data, error}){
        if(data){
            console.log(' Picklist Values ', data.picklistFieldValues.tat__c.values);
            this.pickListvaluesByRecordType = data.picklistFieldValues.tat__c.values;
        }
        if(error){
            console.log(error);
        }
    }
    @wire(getObjectInfo,{
        objectApiName : SOUS_TICKET_OBJECT
    })
        wiredObject({data, error}){
            if(data){
                console.log(' Object iformation ', data);
                console.table(data);
            }
            if(error){
                console.log(error);
            }
        } 
    handleNameSousTicketchange(event){
            this.SousTicketRecord.Name= event.target.value;
            window.console.log('name==>'+this.SousTicketRecord.Name);}
            
    handleDescriptionSousTicketchange(event){
            this.SousTicketRecord.Description__c= event.target.value;
            window.console.log('Description__c==>'+this.SousTicketRecord.Description__c);}
            
    handleetatSousTicketchange(event){
        this.SousTicketRecord.tat__c= event.target.value;
        window.console.log('tat__c==>'+this.SousTicketRecord.tat__c);}

    @api refresh() {
        this.timestamp = new Date();
    }
    openmodal() {
        this.openmodel = true;
    }
    closeModal() {
        this.openmodel = false;
    } 

    saveMethod() {
        saveSousTicketRecord({objAcc: this.SousTicketRecord, TObj: this.DetailTicket})
            .then (result => {
                this.SousTicketRecord={};
            window.console.log('result===>'+result);
           // show sucess message
            this.dispatchEvent(new ShowToastEvent({
                title:'Success!',
                message: 'Under Ticket successfully created!',
                variant: 'Success!',
            }),);
               // refreshing table data using refresh apex
            return refreshApex(result);
            })
            .catch(error => {
                this.error =error.message;
            });
        this.closeModal();
    }
    handleSuccess() {
       return refreshApex(this.refreshTable);
    }
    handleRowActions(event) {
        let actionName = event.detail.action.name;
    
        window.console.log('actionName ====> ' + actionName);
        let row = event.detail.row;
        window.console.log('row ====> ' + row);
        // eslint-disable-next-line default-case
        switch (actionName) {
            case 'Add_Commentaire':
            this.showpage(row);
            break;
            case 'record_details':
                this.viewCurrentRecord(row);
                console.log("row!" +JSON.stringify(row));
                break;
            case 'edit':
                this.editCurrentRecord(row);
                break;
            case 'delete':
                this.deleteSousTicket(row);
                break;
        }
    }
    handleNavigateToProjet(event) {
        event.preventDefault();
        this[NavigationMixin.Navigate]({
            type: 'standard__navItemPage',
            attributes: {
                apiName:   'liste_des_projet',
            },
        });
    }
    toCompleted(){
        sousTicketupdateEtat({Sousticket:this.record.Id , newetat: 'Completed'})
        .then (result => {
        true;
        window.console.log('result===>'+result);
       // show sucess message
        this.dispatchEvent(new ShowToastEvent({
            title:'Success!',
            message: 'Status of Under Ticket is Completed!',
            variant: 'Success!',
        }),);
           // refreshing table data using refresh ape
        })
        .catch(error => {
            this.error =error.message;
        });
    }
    toNotStarted(){
        sousTicketupdateEtat({Sousticket:this.record.Id , newetat: 'Not Started'})
        .then (result => {
        true;
        window.console.log('result===>'+result);
       // show sucess message
        this.dispatchEvent(new ShowToastEvent({
            title:'Success!',
            message: 'Status of Under Ticket is Not Started!',
            variant: 'Success!',
        }),);
           // refreshing table data using refresh ape
        })
        .catch(error => {
            this.error =error.message;
        });
    }
    toInProgress(){
        sousTicketupdateEtat({Sousticket:this.record.Id , newetat: 'In Progress'})
        .then (result => {
        true;
        window.console.log('result===>'+result);
       // show sucess message
        this.dispatchEvent(new ShowToastEvent({
            title:'Success!',
            message: 'Status of Under Ticket is In Progress!',
            variant: 'Success!',
        }),);
        })
        .catch(error => {
            this.error =error.message;
        });
    }
    toDeferred(){
        sousTicketupdateEtat({Sousticket:this.record.Id, newetat: 'Deferred'})
            .then (result => {
            true;
            window.console.log('result===>'+result);
           // show sucess message
            this.dispatchEvent(new ShowToastEvent({
                title:'Success!',
                message: 'Status of Under Ticket is Deferred!',
                variant: 'Success!',
            }),);
               // refreshing table data using refresh ape
            })
            .catch(error => {
                this.error =error.message;
            });
        }
        showpage(currentRow){
            this.showpages=true;
            // const record = event.detail.row;
            //console.log('Ticket',record.tat__c);
            this.idd = currentRow.Id;
            this.record = currentRow;
            console.log('Ticket',this.record.tat__c);
            this.refresh();
                if(this.record.tat__c=='Completed'){
                    this.Icon_Completed = true;  
                    this.Icon_Not_Started = false;  
                    this.Icon_In_Progress = false; 
                    this.Icon_Deferred = false;  
        }
                if(this.record.tat__c=='Not Started'){
                    this.Icon_Completed = false;  
                    this.Icon_Not_Started = true;  
                    this.Icon_In_Progress = false; 
                    this.Icon_Deferred = false;  
        }
                if(this.record.tat__c=='In Progress'){
                    this.Icon_Completed = false;  
                    this.Icon_Not_Started = false;  
                    this.Icon_In_Progress = true; 
                    this.Icon_Deferred = false;  
        }
                if(this.record.tat__c=='Deferred'){
                    this.Icon_Completed = false;  
                    this.Icon_Not_Started = false;  
                    this.Icon_In_Progress = false; 
                    this.Icon_Deferred = true;  
        }
        
    }
    editRecord(){
        this.editCurrentRecord(this.record);
    }
    handleSousTicketNoteschange(event){
        this.Notes__c= event.target.value;
        console.log('Notes==>'+this.Notes__c);
        console.log('Notes==>'+this.record.Notes__c);
        if(this.Notes__c==""){
            this.verification=false;
        }
        else{
            this.verification=true;
        }
        updateNotesSousTicket({Sousticket:this.idd,NewNote: this.Notes__c})
}
tableSousTicket() {
    getSousTicket({TObj: this.DetailTicket})
        .then (result => {
            this.SousTicketRecord={};
            this.data = result;
        })}
viewCurrentRecord(currentRow) {
            this.bShowModal = true;
            this.isEditForm = false;
            this.record = currentRow;
        }
closeModalAction() {
            this.bShowModal = false;
        }
editCurrentRecord(currentRow) {
            // open modal box
            this.bShowModal = true;
            this.isEditForm = true;
            // assign record id to the record edit form
            this.currentRecordId = currentRow.Id;
        }
        handleSubmit(event) {
            // prevending default type sumbit of record edit form
            event.preventDefault();
    
            // querying the record edit form and submiting fields to form
            this.template.querySelector('lightning-record-edit-form').submit(event.detail.fields);
    
            // closing modal
            this.bShowModal = false;
    
            // showing success message
            this.dispatchEvent(new ShowToastEvent({
                title: 'Success!',
                message: event.detail.fields.Name + 'Under Ticket successfully updated!!.',
                variant: 'Success!'
            }),);
        }
        deleteSousTicket(currentRow) {
    let currentRecord = [];
    currentRecord.push(currentRow.Id);
    this.showLoadingSpinner = true;
    // calling apex class method to delete the selected contact
    deleteSousTicket({lstSousTicketIds: currentRecord})
    .then(result => {
        window.console.log('result ====> ' + result);
        this.showLoadingSpinner = false;
        // showing success message
        this.dispatchEvent(new ShowToastEvent({
            title: 'Success!',
            message: currentRow.Name +'Under Ticket deleted.',
            variant: 'Success!'
        }),);
        // refreshing table data using refresh apex
    })
    .catch(error => {
        window.console.log('Error ====> '+error);
        this.dispatchEvent(new ShowToastEvent({
            title: 'Error!!', 
            message: error.message, 
            variant: 'error'            }),);
    });
}
    @wire(CurrentPageReference) wiredPageRef(pageRef) 
    {this.pageRef = pageRef; 
        if(this.pageRef) 
            registerListener('sendmessage', this.handleChange, this); }
connectedCallback() {

    var Ticket= this.pageRef.attributes.recordTicket;
    if (Ticket!= null && Ticket!= undefined)
        {sessionStorage.setItem('DetailTicket',JSON.stringify((Ticket)));
        }
        this.DetailTicket=JSON.parse(sessionStorage.getItem('DetailTicket'));

    var project= this.pageRef.attributes.recordProjet;
                    if (project!= null && project!= undefined )
                            {
                    sessionStorage.setItem('DetailProjet',JSON.stringify((project)));
                            }
                    this.DetailProjet=JSON.parse(sessionStorage.getItem('DetailProjet'));
var UserStory= this.pageRef.attributes.recordUserStory;
                    if (UserStory!= null && UserStory!= undefined )
                            {
                    sessionStorage.setItem('DetailUserStory',JSON.stringify((UserStory)));
                            }
                    this.DetailUserStory=JSON.parse(sessionStorage.getItem('DetailUserStory'));
    this.tableSousTicket();

}
disconnectedCallback() {
// unsubscribe from inputChangeEvent event
unregisterAllListeners(this);

console.log('nom Projet===>'+JSON.stringify(this.DetailProjet.Name));
console.log('nom user===>'+JSON.stringify(this.DetailUserStory.Name));
console.log('nom Ticket===>'+JSON.stringify(this.DetailTicket.Name));


}
}