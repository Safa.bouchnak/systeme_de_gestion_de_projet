import { LightningElement ,wire, track,api } from 'lwc';
import NAME_FIELD from '@salesforce/schema/Ticket__c.Name';
import DESCRIPTION_FIELD from '@salesforce/schema/Ticket__c.Description__c';
import ETAT_FIELD from '@salesforce/schema/Ticket__c.tat__c';
import TEMPSREEL_FIELD from '@salesforce/schema/Ticket__c.Temps_R_el__c';
import TICKET_OBJECT from '@salesforce/schema/Ticket__c';
import TIMENAME_FIELD from '@salesforce/schema/Temps_Consom__c.Name';
import TIMEFIRSTETIME_FIELD from '@salesforce/schema/Temps_Consom__c.Temps_Debut__c';
import { fireEvent } from 'c/pubsub';
import getUserWorkTime from '@salesforce/apex/workTimeUserStory.getUserWorkTime' ;
import updateUserWorkTime from '@salesforce/apex/workTimeUserStory.updateUserWorkTime' 
import getWorkTime from '@salesforce/apex/workTime.getWorkTime';
import updateWorkTime from '@salesforce/apex/workTime.updateWorkTime';
import TIMELASTTIME_FILED from '@salesforce/schema/Temps_Consom__c.Temps_Fin__c';
import DURE_FILED from '@salesforce/schema/Temps_Consom__c.Dur_e__c';
import saveTimeRecord from '@salesforce/apex/timeConsomedControler.saveTimeRecord';
import getTime from '@salesforce/apex/timeControlerLWC.getTime';
import GetElapsedSeconds from '@salesforce/apex/duration.GetElapsedSeconds';
import deleteTime from '@salesforce/apex/timeControlerLWC.deleteTime';
import { getPicklistValues, getPicklistValuesByRecordType, getObjectInfo } from 'lightning/uiObjectInfoApi';
import USERSTORY_FIELD from '@salesforce/schema/Ticket__c.User_Story__c';
import NOTES_FIELD from '@salesforce/schema/Ticket__c.Notes__c';
import SUM_FILED from '@salesforce/schema/Ticket__c.Sum_temps__c';
import updateNotesTicket from '@salesforce/apex/updateNote.updateNotesTicket';
import saveTicketRecord from '@salesforce/apex/ticketControlerLWC.saveTicketRecord';
import getTicket from '@salesforce/apex/ticketControler.getTicket';
import deleteTicket from '@salesforce/apex/ticketControler.deleteTicket';
import updateEtat from '@salesforce/apex/TicketUpdate.updateEtat';
// ***************

import getProjetWorkTime from '@salesforce/apex/workTimeProjet.getProjetWorkTime'; 
import updateProjetWorkTime from '@salesforce/apex/workTimeProjet.updateProjetWorkTime'; 
import getProjetAvencemet from '@salesforce/apex/Avencement.getProjetAvencemet'; 
import updateProjetAvencement from '@salesforce/apex/Avencement.updateProjetAvencement'; 
import getProjetIcon from '@salesforce/apex/IconProjet.getProjetIcon'; 
import updateProjetIcon from '@salesforce/apex/IconProjet.updateProjetIcon'; 
import delayprojet from '@salesforce/apex/delay.delayprojet'; 
import updateProjetdelay from '@salesforce/apex/delay.updateProjetdelay'; 
import { CurrentPageReference } from 'lightning/navigation';
import { registerListener, unregisterAllListeners } from 'c/pubsub';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import {refreshApex} from '@salesforce/apex';
import { NavigationMixin } from 'lightning/navigation';

const columnsTime =[
    {label: 'Name', fieldName: 'Name' },
    { label: 'First Time', fieldName: 'Temps_Debut__c'},
    { label: 'Last Time', fieldName: 'Temps_Fin__c' },
    { label: 'Duration in (min)', fieldName: 'Dur_e__c', type: 'Number'},
    {type: "button-icon", typeAttributes: {  
        label: 'delete',  
        name: 'delete',  
        title: 'delete',  
        disabled: false,  
        value: 'delete',  
        iconName: 'action:delete',
        iconPosition: 'left'  
}}
];
const columns = [
    {type: "button-icon", typeAttributes: {  
        iconName: 'action:share',
        label: 'record_Apex',  
        name: 'record_Apex',  
        title: 'record_Apex',  
        disabled: false,  
        value: 'record_Apex',  
        iconPosition: 'right' , 
    }},
    {label: 'List of Ticket', fieldName: 'Name' },

{type: "button-icon", typeAttributes: {  
    label: 'delete',  
    name: 'delete',  
    title: 'delete',  
    disabled: false,  
    value: 'delete',  
    iconName: 'action:delete',
    iconPosition: 'left'  
}},
{type: "button-icon", typeAttributes: {  
    label: 'Edit',  
    name: 'edit',  
    title: 'edit' , 
    disabled: false,  
    value: 'edit',  
    iconName: 'action:edit',
    iconPosition: 'left'  
}},
{type: "button-icon" , typeAttributes: {  
    label: 'record_details',  
    name: 'record_details',  
    title: 'record_details',  
    disabled: false,  
    value: 'record_details',  
    iconName: 'action:preview',
    iconPosition: 'left'  
    
} },
{type: "button-icon", typeAttributes: {  
    iconName: 'action:share',
    label: 'Add_Commentaire',  
    name: 'Add_Commentaire',  
    title: 'Add_Commentaire',  
    disabled: false,  
    value: 'Add_Commentaire',  
    iconPosition: 'Add_Commentaire' , 
}} ];

export default class listedestache extends NavigationMixin(LightningElement){
selectedRecords = [];
refreshTable;
estimetedTime;
Etat;
_wiredResult;
timestamp = new Date();
@track Icon_Completed=false;
@track Icon_Deferred=false;
@track Icon_Not_Started =false;
@track Icon_In_Progress =false;
@track detail;
@track minutes ;
@track detailTime;
@track showpages =false;
@track bShowModal = false;
@track recordId;
@track verification;
@track isEditForm = false;
@track showLoadingSpinner = false;
@track data;
@track dataTime;
@track note;
@track record = [];
@track error; 
@track openmodel = false;
@track pickListvalues;
@track columns = columns;
@track columnsTime = columnsTime;
@track DetailUserStory ;
@track DetailProjet;
@track values;
@track currentRecordId;
@track idd;
@track tat__c;
@track Notes__c;
@track today;
@track pickListvaluesByRecordType;
@track displayIconName ;
@track timeWork;
@track NoteRecord={
    Notes__c:NOTES_FIELD
}
@track CnsometedTime={
    Name:TIMENAME_FIELD,
    Temps_Debut__c:TIMEFIRSTETIME_FIELD,
    Temps_Fin__c:TIMELASTTIME_FILED,
    Dur_e__c:DURE_FILED,
   
}
@track TicketRecord={
    Name : NAME_FIELD,
    Description__c :DESCRIPTION_FIELD,
    tat__c:ETAT_FIELD,
    Temps_R_el__c:TEMPSREEL_FIELD,
    User_Story__c:USERSTORY_FIELD,
    Sum_temps__c:SUM_FILED,
}

@wire(getPicklistValues, {
    recordTypeId : '012000000000000AAA',
    fieldApiName : ETAT_FIELD,
})
    wiredPickListValue({ data, error }){
        if(data){
            console.log(` Picklist values are `, data. values);
            this.pickListvalues = data.values;
            this.error = undefined;
        }
        if(error){
            console.log(` Error while fetching Picklist values  ${error}`);
            this.error = error;
            this.pickListvalues = undefined;
        }
    }
    @wire(getPicklistValuesByRecordType, {
        recordTypeId : '01I3z000001EgoA',
        objectApiName : TICKET_OBJECT
    })
    wiredRecordtypeValues({data, error}){
        if(data){
            console.log(' Picklist Values ', data.picklistFieldValues.tat__c.values);
            this.pickListvaluesByRecordType = data.picklistFieldValues.tat__c.values;
        }
        if(error){
            console.log(error);
        }
    }
    @wire(getObjectInfo,{
        objectApiName : TICKET_OBJECT
    })
        wiredObject({data, error}){
            if(data){
                console.log(' Object iformation ', data);
                console.table(data);
            }
            if(error){
                console.log(error);
            }
        } 
    handleNameTicketchange(event){
            this.TicketRecord.Name= event.target.value;
            window.console.log('name==>'+this.TicketRecord.Name);}
            
    handleDescriptionchange(event){
            this.TicketRecord.Description__c= event.target.value;
            window.console.log('Description__c==>'+this.TicketRecord.Description__c);}
            
    handleetatchange(event){
        this.TicketRecord.tat__c= event.target.value;
        window.console.log('tat__c==>'+this.TicketRecord.tat__c);}
    
    handleTempschange(event){
        this.TicketRecord.Temps_R_el__c= event.target.value;
        window.console.log('Temps_R_el__c==>'+this.TicketRecord.Temps_R_el__c);}

        
    handleTempsNamechange(event){
        this.CnsometedTime.	Name= event.target.value;
        window.console.log('name==>'+this.CnsometedTime.Name);}
        
    handleFirstTimechange(event){
        this.CnsometedTime.Temps_Debut__c= event.target.value;
        window.console.log('Temps_Debut__c==>'+this.CnsometedTime.Temps_Debut__c);
}


    handleFinTimechange(event){
        this.CnsometedTime.Temps_Fin__c= event.target.value;
        window.console.log('Temps_Fin__c==>'+this.CnsometedTime.Temps_Fin__c);}
    @api refresh() {
        this.timestamp = new Date();
    }
    openmodal() {
        this.openmodel = true;
    }
    closeModal() {
        this.openmodel = false;
    } 

    saveMethod() {
        saveTicketRecord({objAcc: this.TicketRecord, USObj: this.DetailUserStory})
            .then (result => {
                this.TicketRecord ={};
            window.console.log('result===>'+result);
           // show sucess message
            this.dispatchEvent(new ShowToastEvent({
                title:'Succès!',
                message: 'Ticket créé avec succès!',
                variant: 'success',
            }),);
               // refreshing table data using refresh apex
            return refreshApex(result);
            })
            .catch(error => {
                this.error =error.message;
            });
        this.closeModal();
    }
   
    handleSuccess() {
        return refreshApex(this.record);
    }
     @wire(CurrentPageReference) pageRef;
    navigateTolisteSousTache(event) {
    const record = event.detail.row;
        fireEvent(this.pageRef, 'sendmessage',{story: record.Id});
      

console.log('nom Projet===>'+JSON.stringify(this.recordTicket));
console.log('nom user===>'+JSON.stringify(this.recordUserStory));
console.log('nom Ticket===>'+JSON.stringify(this.recordProjet));


        this[NavigationMixin.Navigate]({
            type: 'standard__navItemPage',
            attributes: {
                apiName: 'liste_des_sous_tickets',
                recordTicket: record,
                recordUserStory :this.DetailUserStory,
                 recordProjet:this.DetailProjet,
            },
        });
    }

    handleRowActions(event) {
        let actionName = event.detail.action.name;
    
        window.console.log('actionName ====> ' + actionName);
        let row = event.detail.row;
        window.console.log('row ====> ' + row);
        // eslint-disable-next-line default-case
        switch (actionName) {
            case 'record_Apex':
                this.navigateTolisteSousTache(event);
                break;
            case 'Add_Commentaire':
            this.showpage(row);
            break;
            case 'record_details':
                this.viewCurrentRecord(row);
                console.log("row!" +JSON.stringify(row));
                break;
            case 'edit':
                this.editCurrentRecord(row);
                break;
            case 'delete':
                this.deleteTicket(row);
                break;
        }
    }
    handleRowTimeActions(event) {
        let actionName= event.detail.action.name;
        window.console.log('actionNameTime ====> ' + actionName);
        let row= event.detail.row;
        window.console.log('Time ====> ' + row);
        // eslint-disable-next-line default-case
        switch (actionName) {
            case 'delete':
                this.deleteTime(row);
                break;
        }
    }
    handleNavigateToProjet(event) {
        event.preventDefault();
        this[NavigationMixin.Navigate]({
            type: 'standard__navItemPage',
            attributes: {
                apiName:   'liste_des_projet',
            },
        });
    }
    toCompleted(){
        updateEtat({ticket:this.record.Id , newetat: 'Completed'})
        .then (result => {
        true
        window.console.log('result===>'+result);
       // show sucess message
        this.dispatchEvent(new ShowToastEvent({
            title:'Succès!',
            message: 'Status of Ticket is Completed!',
            variant: 'success',
        }),);
           // refreshing table data using refresh ape
        })
        .catch(error => {
            this.error =error.message;
        });
    }
    toNotStarted(){
        updateEtat({ticket:this.record.Id , newetat: 'Not Started'})
        .then (result => {
        true;
        window.console.log('result===>'+result);
       // show sucess message
        this.dispatchEvent(new ShowToastEvent({
            title:'Succès!',
            message: 'Status of Ticket is Not Started!',
            variant: 'success',
        }),);
           // refreshing table data using refresh ape
        })
        .catch(error => {
            this.error =error.message;
        });
    }
    toInProgress(){
        updateEtat({ticket:this.record.Id , newetat: 'In Progress'})
        .then (result => {
        true;
        window.console.log('result===>'+result);
       // show sucess message
        this.dispatchEvent(new ShowToastEvent({
            title:'Succès!',
            message: 'Status of Ticket is In Progress!',
            variant: 'success',
        }),);
        })
        .catch(error => {
            this.error =error.message;
        });
    }
    toDeferred(){
            updateEtat({ticket:this.record.Id , newetat: 'Deferred'})
            .then (result => {
            true;
            window.console.log('result===>'+result);
           // show sucess message
            this.dispatchEvent(new ShowToastEvent({
                title:'Succès!',
                message: 'Status of Ticket is Deferred!',
                variant: 'success',
            }),);
               // refreshing table data using refresh ape
            })
            .catch(error => {
                this.error =error.message;
            });
        }

        showpage(currentRow){
            this.showpages=true;
            this.idd = currentRow.Id;
            this.record = currentRow;
            console.log('TicketID',JSON.stringify(this.record));
            getWorkTime({TObj:this.record})
        .then (result => {
            this.timeWork = (Math.floor(Number(result) / 60))+' h:'+( Number(result) % 60)+'min';
            console.log('resultTimeAffich===>',result);
            updateWorkTime({TObj:this.record,SumTim: result})
            .then (result => {
                console.log('update===>',result);
            })
        })
            console.log('Ticket',this.record.tat__c);
            this.refresh();
                if(this.record.tat__c=='Completed'){
                    this.Icon_Completed = true;  
                    this.Icon_Not_Started = false;  
                    this.Icon_In_Progress = false; 
                    this.Icon_Deferred = false;  
        }
                if(this.record.tat__c=='Not Started'){
                    this.Icon_Completed = false;  
                    this.Icon_Not_Started = true;  
                    this.Icon_In_Progress = false; 
                    this.Icon_Deferred = false;  
        }
                if(this.record.tat__c=='In Progress'){
                    this.Icon_Completed = false;  
                    this.Icon_Not_Started = false;  
                    this.Icon_In_Progress = true; 
                    this.Icon_Deferred = false;  
        }
                if(this.record.tat__c=='Deferred'){
                    this.Icon_Completed = false;  
                    this.Icon_Not_Started = false;  
                    this.Icon_In_Progress = false; 
                    this.Icon_Deferred = true;  
        }
        
    }
     // modale add time
    @track openmodelTemps = false;
        openmodalTemps() {
            this.openmodelTemps = true
            this.tableTime();

    }
    closeModalTemps() {
        this.openmodelTemps = false
    } 
  
    saveMethodTemps() {
     //   console.log('aaaaaaa===>'+this.record.Id);
        getWorkTime({TObj:this.record})
        .then (result => {
            this.timeWork = (Math.floor(Number(result) / 60))+' h:'+( Number(result) % 60)+'min';
            console.log('resultTimeAffich===>',result);
            updateWorkTime({TObj:this.record,SumTim: result})
            .then (result => {
                console.log('update===>',result);
                this.AvencementProjet();
            })
        })
        this.closeModalTemps();
}
    @track openmodelAdd = false;
        openmodalAdd() {
            this.openmodelAdd = true
    }
    closeModalAdd() {
        this.openmodelAdd = false
    } 
    saveMethodAdd() {
        console.log('IDTichek'+JSON.stringify(this.record))
        console.log('consomed===>'+JSON.stringify( this.CnsometedTime));
        GetElapsedSeconds({TD:this.CnsometedTime.Temps_Debut__c ,TF:this. CnsometedTime.Temps_Fin__c})
        .then (result => {
            this.CnsometedTime.Dur_e__c=result;
        saveTimeRecord({objAcc: this.CnsometedTime, TicketObj: this.record})
        .then (result => {
                this.CnsometedTime ={};

            window.console.log('result===>'+result);
        // show sucess message
        this.dispatchEvent(new ShowToastEvent({
            title:'Succès!',
            message: 'Time créé avec succès!',
            variant: 'success',
        }),);
        })})
        .catch(error => {
            this.error =error.message;
        });
        this.closeModalAdd();

    }
    AvencementProjet(){
        getUserWorkTime({USObj:this.DetailUserStory})
        .then (result => {
            console.log('resultTimeAffich===>',result);
            updateUserWorkTime({USObj:this.DetailUserStory,SumTim: result})
            .then (result => {
                console.log('update===>',result);
            })
        })
        delayprojet({
            ProjetID:this.DetailProjet.Id
        }).then(result => {
            console.log('Delay' + result)
            updateProjetdelay({
                PObj: this.DetailProjet,
                delay: result
            })
        })
        getProjetAvencemet({
            ProjetID:this.DetailProjet.Id
        }).then(result => {
            console.log('avencement' + result)
            updateProjetAvencement({
                PObj: this.DetailProjet,
                SumUserCompleted: result
            })
        })
        getProjetWorkTime({
            ProjetID: this.DetailProjet.Id
        }).then(result => {

            console.log('time' + result)
            updateProjetWorkTime({
                PObj:this.DetailProjet,
                SumTim: result
            })
        })
        getProjetIcon({
            ProjetID: this.DetailProjet.Id
        }).then(result => {
            console.log('Icon' + result)
            updateProjetIcon({
                PObj:this.DetailProjet,
                icon: result
            })
        })
    }
    editRecord(){
        this.editCurrentRecord(this.recPord);
    }
    handleNoteschange(event){
        this.Notes__c= event.target.value;
        console.log('Notes==>'+this.Notes__c);
        console.log('Notes==>'+this.record.Notes__c);
        if(this.Notes__c==""){
            this.verification=false;
        }
        else{
            this.verification=true;
        }
        updateNotesTicket({ticket:this.idd,NewNote: this.Notes__c})
}
tableTime() {
    getTime({TicketObj:this.record})
            .then (result => {
            this.CnsometedTime={};
            this.dataTime = result;
            console.log('ttt'+JSON.stringify(this.dataTime));
        })}
    tableTicket() {
        getTicket({USObj: this.DetailUserStory})
            .then (result => {
                this.TicketRecord={};
                this.data = result;
            })}
    viewCurrentRecord(currentRow) {
                this.bShowModal = true;
                this.isEditForm = false;
                this.record = currentRow;
            }
    closeModalAction() {
                this.bShowModal = false;
            }
    editCurrentRecord(currentRow) {
                // open modal box
                this.bShowModal = true;
                this.isEditForm = true;
                // assign record id to the record edit form
                this.currentRecordId = currentRow.Id;
            }
            handleSubmit(event) {
                // prevending default type sumbit of record edit form
                event.preventDefault();
        
                // querying the record edit form and submiting fields to form
                this.template.querySelector('lightning-record-edit-form').submit(event.detail.fields);
        
                // closing modal
                this.bShowModal = false;
        
                // showing success message
                this.dispatchEvent(new ShowToastEvent({
                    title: 'Succès!',
                    message: +'Ticket mis à jour avec succès !!.',
                    variant: 'success'
                }),);
        
            }
            
    deleteTicket(currentRow) {
        let currentRecord = [];
        currentRecord.push(currentRow.Id);
        this.showLoadingSpinner = true;
        // calling apex class method to delete the selected contact
        deleteTicket({lstTicketIds: currentRecord})
        .then(result => {
            window.console.log('result ====> ' + result);
            this.showLoadingSpinner = false;
            // showing success message
            this.dispatchEvent(new ShowToastEvent({
                title: 'Succès!',
                message: currentRow.Name +'Ticket supprimé.',
                variant: 'success'
            }),);
            // refreshing table data using refresh apex
            return refreshApex(this._wiredResult);
        })
        .catch(error => {
            window.console.log('Error ====> '+error);
            this.dispatchEvent(new ShowToastEvent({
                title: 'Error!!', 
                message: error.message, 
                variant: 'error'            }),);
        });
    }
    deleteTime(currentRow) {
        let currentRecord = [];
        currentRecord.push(currentRow.Id);
        this.showLoadingSpinner = true;
        // calling apex class method to delete the selected contact
        deleteTime({lstTimeIds: currentRecord})
        .then(result => {
            window.console.log('result ====> ' + result);
            this.showLoadingSpinner = false;
            // showing success message
            this.dispatchEvent(new ShowToastEvent({
                title: 'Succès!',
                message: currentRow.Name +'Time supprimé.',
                variant: 'success'
            }),);
        })
        .catch(error => {
            window.console.log('Error ====> '+error);
            this.dispatchEvent(new ShowToastEvent({
                title: 'Error!!', 
                message: error.message, 
                variant: 'error'            }),);
        });
    }
    @wire(CurrentPageReference) wiredPageRef(pageRef) 
        {this.pageRef = pageRef; 
            if(this.pageRef) 
                registerListener('sendmessage', this.handleChange, this); }
    connectedCallback() {
    var Sprint = this.pageRef.attributes.recordSprint;
        if (Sprint!= null && Sprint!= undefined)
            {sessionStorage.setItem('detailSprint',JSON.stringify((Sprint)));
            }
    this.detailSprint=JSON.parse(sessionStorage.getItem('detailSprint'));

    var project= this.pageRef.attributes.recordProjet;
                    if (project!= null && project!= undefined )
                            {
                    sessionStorage.setItem('DetailProjet',JSON.stringify((project)));
                            }
                    this.DetailProjet=JSON.parse(sessionStorage.getItem('DetailProjet'));

    var UserStory= this.pageRef.attributes.recordUserStory;
                    if (UserStory!= null && UserStory!= undefined )
                            {
                    sessionStorage.setItem('DetailUserStory',JSON.stringify((UserStory)));
                            }
                    this.DetailUserStory=JSON.parse(sessionStorage.getItem('DetailUserStory'))
        this.tableTicket();
        return refreshApex(this.refreshTable);
}
disconnectedCallback() {
// unsubscribe from inputChangeEvent event
unregisterAllListeners(this);
console.log('nom Projet===>'+JSON.stringify(this.DetailProjet.Name));
}
}