Cette application permet de superviser le travail de tous les acteurs partant d’une vue globale
jusqu’à parvenir au moindre détail concernant le projet.
De plus, l’application doit être capable de travailler en temps réel afin de pouvoir contrôler
l’avancement des tâches de la manière la plus efficace possible et bien gérer le temps de
travail par projet .
En effet, la mise à jour du statut de progrès des tâches auprès de leurs réalisateurs permet à un
chef de projet, ainsi que le membre d’équipe même, de poursuivre l’avancement.
Dans ce cadre, nous visons à travers l’application à réaliser et à atteindre les objectifs
suivants :
 Créer une liste de sprint à partir d’un backlog pour satisfaire les exigences côté client.
 Créer une liste de user story pour chaque sprint.
 Créer une liste des tâches composants l’un des sprints du backlog.
 Créer et surveiller l’avancement de l’équipe
 Créer et planifier un projet
 Effectuer les différentes missions au sein d’une équipe
 Faciliter la coordination et communication entre les différents membres de l’équipe
 Faire interagir le client.